import { addDecorator } from '@storybook/react';

import theme from '../utils/theme';
import React from 'react';
import { ThemeProvider as ThemeProviderEmotion } from 'emotion-theming';
import { jsxDecorator } from 'storybook-addon-jsx';

const ThemeDecorator = (storyFn) => <ThemeProviderEmotion theme={theme}>{storyFn()}</ThemeProviderEmotion>;

addDecorator(jsxDecorator);
addDecorator(ThemeDecorator);
