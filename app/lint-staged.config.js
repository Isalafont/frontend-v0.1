module.exports = {
  linters: {
    '**/*.{js,jsx,ts,tsx}': ['jest --findRelatedTests'],
  },
};
