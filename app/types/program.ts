import { UsersSm } from './common';

export interface Program {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_title_fr?: any;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  status: string;
  users_sm: UsersSm[];
  feed_id: number;
  short_description: string;
  short_description_fr?: string;
  faq?: string;
  faq_fr?: string;
  enablers?: any;
  enablers_fr?: any;
  ressources?: any;
  launch_date?: any;
  end_date?: any;
  claps_count: number;
  follower_count: number;
  saves_count: number;
  members_count: number;
  needs_count: number;
  projects_count: number;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  has_clapped: boolean;
  has_followed: boolean;
  has_saved: boolean;
  description: string;
  description_fr?: any;
}
