export interface Faq {
  id: number;
  title: string;
  content: string;
}
