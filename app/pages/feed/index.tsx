import { useIntl } from 'react-intl';
import Box from '~/components/Box';
import Feed from '~/components/Feed/Feed';
import Layout from '~/components/Layout';
import useUserData from '~/hooks/useUserData';

const GeneralFeed = () => {
  const { userData } = useUserData();
  const { formatMessage } = useIntl();
  return (
    <Layout>
      <Box alignItems="center">
        <h3 style={{ marginBottom: '20px' }}>
          {formatMessage({
            id: 'feed.all',
            defaultMessage: 'General JOGL feed',
          })}
        </h3>
        <Feed feedId="all" displayCreate={!!userData} isAdmin={false} />
      </Box>
    </Layout>
  );
};
export default GeneralFeed;
