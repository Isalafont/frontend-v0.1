import { useState, useContext } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Router from 'next/router';
import CommunityForm from '~/components/Community/CommunityForm';
import Layout from '~/components/Layout';
import { UserContext } from '~/contexts/UserProvider';
import { useApi } from '~/contexts/apiContext';
import { NextPage } from 'next';
import ReactGA from 'react-ga';

interface Props {}
const CreatePage: NextPage<Props> = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const [newCommunity, setNewCommunity] = useState({
    creator_id: Number(userContext.credentials.userId),
    interests: [],
    short_description: '',
    short_title: '',
    skills: [],
    resources: [],
    title: '',
    is_private: false,
  });
  const [sending, setSending] = useState(false);
  const intl = useIntl();

  const handleChange = (key, content) => {
    setNewCommunity((prevState) => ({ ...prevState, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .post('/api/communities/', { community: newCommunity })
      .then((res) => {
        setSending(false);
        // record event to Google Analytics
        ReactGA.event({ category: 'Group', action: 'create', label: `${res.data.id}` /* proj id */ });
        // go to created group edition page
        Router.push('/community/[id]/edit', `/community/${res.data.id}/edit`);
      })
      .catch(() => {
        setSending(false);
      });
  };
  return (
    <Layout
      title={`${intl.formatMessage({ id: 'community.create.title', defaultMessage: 'Create a new group' })} | JOGL`}
    >
      <div className="communityCreate container-fluid">
        <h1>
          <FormattedMessage id="community.create.title" defaultMessage="Create a new group" />
        </h1>
        <CommunityForm
          community={newCommunity}
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          mode="create"
          sending={sending}
        />
      </div>
    </Layout>
  );
};
export default CreatePage;
