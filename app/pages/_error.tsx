// TODO: Wait until
import Router from 'next/router';
import React from 'react';

function Error({ statusCode }) {
  return <p>{statusCode ? `An error ${statusCode} occurred on server` : 'An error occurred on client'}</p>;
}
Error.getInitialProps = async ({ res, err, asPath }) => {
  // Capture 404 of pages with traling slash and redirect them
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  // If the page has a trailing slash like "/about/" it will remove the trailing slash
  // so => "/about" while keeping the URL params. This is because nextJS doesn't allow
  // trailing slashes
  // TODO: might have to change it when rewrites and redirects will be available in next.
  if (statusCode && statusCode === 404) {
    const [path, query = ''] = asPath.split('?');
    if (path.match(/\/$/)) {
      const withoutTrailingSlash = path.substr(0, path.length - 1);
      if (res) {
        res.writeHead(302, {
          Location: `${withoutTrailingSlash}${query ? `?${query}` : ''}`,
        });
        res.end();
      } else {
        Router.push(`${withoutTrailingSlash}${query ? `?${query}` : ''}`);
      }
    }
  }
  return { statusCode };
};
export default Error;
