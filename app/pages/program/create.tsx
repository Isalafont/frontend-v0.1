import { useState, useContext, useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Router from 'next/router';
import { UserContext } from '~/contexts/UserProvider';
import Layout from '~/components/Layout';
import ProgramForm from '~/components/Program/ProgramForm';
import { useApi } from '~/contexts/apiContext';
import { NextPage } from 'next';

interface Props {}
const CreateProgram: NextPage<Props> = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const { formatMessage } = useIntl();
  const [newProgram, setNewProgram] = useState({
    title: '',
    short_title: '',
    logo_url: '',
    description: '',
    short_description: '',
    creator_id: Number(userContext.credentials.userId),
    status: 'draft',
    interests: [],
    skills: [],
    banner_url: '',
    is_private: false,
  });

  const handleChange = (key, content) => {
    setNewProgram((prevProgram) => ({ ...prevProgram, [key]: content }));
  };

  const handleSubmit = () => {
    api
      .post('/api/programs/', { program: newProgram })
      .then((res) => {
        Router.push('/program/[id]/edit', `/program/${res.data.id}/edit`);
      })
      .catch(() => {});
  };
  useEffect(() => {
    // TODO redirect program creation to homepage (TEMPORARY FIX!)
    const forbidden = true;
    if (forbidden) {
      Router.push('/', '/');
    }
  }, []);

  return (
    <Layout title={`${formatMessage({ id: 'program.create.title', defaultMessage: 'Create a new program' })} | JOGL`}>
      <div className="programCreate container-fluid">
        <h1>
          <FormattedMessage id="program.create.title" defaultMessage="Create a new program" />
        </h1>
        <ProgramForm mode="create" program={newProgram} handleChange={handleChange} handleSubmit={handleSubmit} />
      </div>
    </Layout>
  );
};
export default CreateProgram;
