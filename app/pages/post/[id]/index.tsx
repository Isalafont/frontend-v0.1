import { NextPage } from 'next';
import React from 'react';
import Box from '~/components/Box';
import PostDisplay from '~/components/Feed/Posts/PostDisplay';
import Layout from '~/components/Layout';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Post } from '~/types';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';

interface Props {
  post: Post;
}
const PostSingleDisplay: NextPage<Props> = ({ post: postProp }) => {
  const { data: post, revalidate: revalidatePost } = useGet('/api/posts/' + postProp.id, { initialData: postProp });
  const { userData } = useUserData();

  return (
    <Layout
      title={`Post from ${post.from.object_type} ${post.from.object_name} | JOGL`}
      desc={`${post.creator.first_name} ${post.creator.last_name}: ${post?.content}`}
    >
      {post && (
        <Box px={2} maxWidth="800px" width="100%" margin="auto">
          <PostDisplay post={post} user={userData} isSingle refresh={revalidatePost} />
        </Box>
      )}
    </Layout>
  );
};
PostSingleDisplay.getInitialProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/posts/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { post: res.data };
  isomorphicRedirect(ctx, '/404', '/404');
};

export default PostSingleDisplay;
