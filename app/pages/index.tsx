import { useState, useEffect, useContext } from 'react';
import Link from 'next/link';
import { useIntl, FormattedMessage } from 'react-intl';
import { UserContext } from '~/contexts/UserProvider';
import MyFeed from '~/components/Feed/MyFeed';
import ProjectList from '~/components/Project/ProjectList';
import CommunityList from '~/components/Community/CommunityList';
import Loading from '~/components/Tools/Loading';
import Layout from '~/components/Layout';
import { NextPage } from 'next';
import useGet from '~/hooks/useGet';
import ProgramCard from '~/components/Program/ProgramCard';
import useUserData from '~/hooks/useUserData';
import Grid from '~/components/Grid';
import NeedCard from '~/components/Need/NeedCard';
import A from '~/components/primitives/A';
import Box from '~/components/Box';

interface PageProps {}
const Home: NextPage<PageProps> = () => {
  const userContext = useContext(UserContext);
  const { userData, userDataError } = useUserData();

  // User can be connected and not having user data.
  if (userContext.isConnected) {
    if (userData) {
      return <HomeConnected feedId={userData.feed_id} />;
    } else {
      // eslint-disable-next-line @rushstack/no-null
      return null;
    }
  }
  return <HomeNotConnected />;
};

const HomeConnected = ({ feedId }) => {
  const { formatMessage, locale } = useIntl();
  const [program, setProgram] = useState();
  const { data: projects, error: projectsError } = useGet('/api/projects?items=4&order=desc');
  const { data: communities, error: communitiesError } = useGet('/api/communities?items=4&order=desc');
  const { data: needs, error: needsError } = useGet('/api/needs?items=4&order=desc');
  const { data: programs, error: programsError } = useGet('/api/programs');
  useEffect(() => {
    if (programs) {
      // get covid19 program (id 2), or first program (id 1, if env is not production)
      const dev = process.env.NODE_ENV !== 'production';
      const featuredProgramId = dev ? 1 : 2;
      setProgram(programs.filter((program) => program.id === featuredProgramId)[0]);
    }
  }, [programs]);
  return (
    <Layout>
      <div className="connectedHome d-flex container-fluid">
        <nav className="nav nav-tabs">
          <a className="nav-item nav-link active" href="#feed" data-toggle="tab">
            <h5>
              <FormattedMessage id="feed.title" defaultMessage="My Feed" />
            </h5>
          </a>
          <a className="nav-item nav-link" href="#latest" data-toggle="tab">
            <h5>
              <FormattedMessage id="home.latest" defaultMessage="What's new" />
            </h5>
          </a>
        </nav>

        <div className="tabContainer">
          <div className="tab-content row">
            <div className="tab-pane active col-12 col-md-8 col-lg-6" id="feed">
              <MyFeed displayCreate feedId={feedId} />
            </div>
            <div className="tab-pane active col-12 col-md-4 col-lg-6" id="latest">
              <div className="justify-content-center">
                {/* Featured Program */}
                <h3>
                  <FormattedMessage id="home.featuredProgram" defaultMessage="Featured program" />
                </h3>
                {program ? (
                  <ProgramCard
                    id={program.id}
                    short_title={program.short_title}
                    title={program.title}
                    title_fr={program.title_fr}
                    short_description={program.short_description}
                    short_description_fr={program.short_description_fr}
                    banner_url={program.banner_url || '/images/default/default-program.jpg'}
                    has_saved={program.has_saved}
                    cardFormat="compact"
                  />
                ) : (
                  <Loading />
                )}
                {/* Latest needs */}
                <div className="latestNeeds">
                  <h3 style={{ margin: '30px 0 0' }}>
                    <FormattedMessage id="needs.latest" defaultMessage="Latest needs" />
                  </h3>
                  {needs ? (
                    <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} pt={3}>
                      {[...needs].reverse().map((need, i) => (
                        <NeedCard
                          key={i}
                          title={need.title}
                          project={need.project}
                          hasSaved={need.has_saved}
                          id={need.id}
                          cardFormat="compact"
                        />
                      ))}
                    </Grid>
                  ) : (
                    <Loading />
                  )}
                  <Box pt={1} pb={4}>
                    <A href="/search/[active-index]" as="/search/needs" passHref>
                      <FormattedMessage id="home.viewMoreRecent" defaultMessage="browse more" />
                    </A>
                  </Box>
                </div>
                {/* Latest projects */}
                <div className="projectsList">
                  <h3 style={{ margin: '10px 0 0' }}>
                    <FormattedMessage id="projects.latest" defaultMessage="Latest projects" />
                  </h3>
                  {projects && <ProjectList listProjects={projects} gridCols={[1, 2, 1, 2]} cardFormat="compact" />}
                  <Box pt={1} pb={4}>
                    <A href="/search/[active-index]" as="/search/projects" passHref>
                      <FormattedMessage id="home.viewMoreRecent" defaultMessage="View more" />
                    </A>
                  </Box>
                </div>
                {/* Latest groups */}
                <div className="communityList">
                  <h3 style={{ margin: '10px 0 0' }}>
                    <FormattedMessage id="communities.latest" defaultMessage="Latest communities" />
                  </h3>
                  {communities ? (
                    <CommunityList listCommunities={communities} gridCols={[1, 2, 1, 2]} cardFormat="compact" />
                  ) : (
                    <Loading />
                  )}
                  <Box pt={1} pb={4}>
                    <A href="/search/[active-index]" as="/search/groups" passHref>
                      <FormattedMessage id="home.viewMoreRecent" defaultMessage="View more" />
                    </A>
                  </Box>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

const HomeNotConnected = () => {
  const { formatMessage } = useIntl();
  return (
    <Layout>
      <div className="Home">
        <div className="heading d-flex justify-content-center">
          <div className="container-fluid aboutJOGL">
            <h1>Just One Giant Lab</h1>
            <div className="aboutText">
              <p>
                {formatMessage({ id: 'home.intro.1', defaultMessage: 'JOGL is all about' })}{' '}
                <strong>
                  {formatMessage({ id: 'home.intro.2', defaultMessage: 'learning and solving together' })}
                </strong>{' '}
                {formatMessage({ id: 'home.intro.3', defaultMessage: 'in the digital age' })}
              </p>
              <p>
                {formatMessage({
                  id: 'home.intro.4',
                  defaultMessage: 'By breaking the walls of traditional institutions',
                })}
                <br />
                {formatMessage({ id: 'home.intro.5', defaultMessage: 'and by' })}{' '}
                <strong>
                  {formatMessage({ id: 'home.intro.6', defaultMessage: 'enhancing collective intelligence' })}
                </strong>
              </p>
              <p>
                {formatMessage({ id: 'home.intro.7', defaultMessage: 'we wish to foster the use of ' })}
                <strong>{formatMessage({ id: 'home.intro.8', defaultMessage: 'Open Science & Technology' })}</strong>
                <br />
                {formatMessage({ id: 'home.intro.9', defaultMessage: 'to tackle' })}{' '}
                <strong>{formatMessage({ id: 'home.intro.10', defaultMessage: 'Humanity’s challenges' })}</strong>
              </p>
              <p className="aboutJoin">
                {formatMessage({ id: 'home.intro.11', defaultMessage: 'Be part of it' })}
                <span>
                  <Link href="/signup" as="/signup">
                    <a>
                      <button type="button" className="btn btn-primary btn-home text-light font-weight-bold">
                        {formatMessage({ id: 'home.intro.join', defaultMessage: 'Join the movement' })}
                      </button>
                    </a>
                  </Link>
                </span>
              </p>
            </div>
          </div>
        </div>

        <div className="container-fluid d-block content text-center">
          <hr />
          <h2>
            <FormattedMessage id="home.join.title" defaultMessage="Join the movement" />
          </h2>
          <p className="joinMouv">
            <FormattedMessage
              id="home.join.text"
              defaultMessage="Come challenge yourself by fostering humanity’s open knowledge and developing solutions to the Sustainable Development Goals."
            />
          </p>
          <div className="cards row text-center justify-content-around">
            <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
              <div className="cardContent">
                <img src="/images/default/default-project.jpg" alt="Projects" />
                <h3>
                  <FormattedMessage id="general.projects" defaultMessage="Projects" />
                </h3>
                <p>
                  <FormattedMessage
                    id="home.projects.text"
                    defaultMessage="You want to change the world? Create your project or join existing ones."
                  />
                </p>
                <Link href="/search/[active-index]" as="/search/projects">
                  <a>
                    <button type="button" className="btn btn-primary">
                      <FormattedMessage id="home.projects.Btn" defaultMessage="Browse" />
                    </button>
                  </a>
                </Link>
              </div>
            </div>
            <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
              <div className="cardContent">
                <img src="/images/default/default-user.jpg" alt="Members" />
                <h3>
                  <FormattedMessage id="general.members" defaultMessage="Members" />
                </h3>
                <p>
                  <FormattedMessage
                    id="home.people.text"
                    defaultMessage="Meet wonderful people that will help you breaking challenges."
                  />
                </p>
                <Link href="/search/[active-index]" as="/search/members">
                  <a>
                    <button type="button" className="btn btn-primary">
                      <FormattedMessage id="home.people.Btn" defaultMessage="Browse" />
                    </button>
                  </a>
                </Link>
              </div>
            </div>
            <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
              <div className="cardContent">
                <img src="/images/default/default-group.jpg" alt="Communities" />
                <h3>
                  <FormattedMessage id="general.groups" defaultMessage="Communities" />
                </h3>
                <p>
                  <FormattedMessage
                    id="home.community.text"
                    defaultMessage="Make your actions and needs visible and connect to inspiring communities."
                  />
                </p>
                <Link href="/search/[active-index]" as="/search/groups">
                  <a>
                    <button className="btn btn-primary" type="button">
                      <FormattedMessage id="home.community.Btn" defaultMessage="Join" />
                    </button>
                  </a>
                </Link>
              </div>
            </div>
            <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
              <div className="cardContent">
                <img src="/images/default/default-challenge-old.jpg" alt="Challenges" />
                <h3>
                  <FormattedMessage id="general.challenges" defaultMessage="Challenges" />
                </h3>
                <p>
                  <FormattedMessage
                    id="home.challenges.text"
                    defaultMessage="Create, join teams and contribute to cracking challenges."
                  />
                </p>
                <Link href="/search/[active-index]" as="/search/challenges">
                  <a>
                    <button type="button" className="btn btn-primary">
                      <FormattedMessage id="home.challenges.Btn" defaultMessage="Compete" />
                    </button>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default Home;
