import { useState, useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import $ from 'jquery';
import { useRouter } from 'next/router';
import { getApiFromCtx } from '~/utils/getApi';
import Feed from '~/components/Feed/Feed';
import Loading from '~/components/Tools/Loading';
import CommunityList from '~/components/Community/CommunityList';
import InfoAddressComponent from '~/components/Tools/Info/InfoAddressComponent';
import InfoDefaultComponent from '~/components/Tools/Info/InfoDefaultComponent';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';
import ProjectList from '~/components/Project/ProjectList';
import UserHeader from '~/components/User/UserHeader';
import Alert from '~/components/Tools/Alert';
import useUserData from '~/hooks/useUserData';
import Layout from '~/components/Layout';
import { linkify, scrollToActiveTab, stickyTabNav } from '~/utils/utils';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import { useApi } from '~/contexts/apiContext';
import Box from '~/components/Box';
import { textWithPlural } from '~/utils/managePlurals';
import TitleInfo from '~/components/Tools/TitleInfo';
import Grid from '~/components/Grid';
import styled from '~/utils/styled';
import useGet from '~/hooks/useGet';
import Chips from '~/components/Chip/Chips';
import { useTheme } from 'emotion-theming';
// import "./UserProfile.scss";
const defaultUser = {
  id: undefined,
  email: '',
  first_name: '',
  last_name: '',
  nickname: '',
  age: '',
  category: '',
  affiliation: '',
  country: '',
  city: '',
  address: '',
  phone_number: '',
  bio: '',
  short_bio: '',
  sign_in_count: 0,
  confirmed_at: '',
  interests: [],
  skills: [],
  resources: [],
  projects_count: 0,
};
export default function UserProfile({ user = defaultUser }) {
  const [listProjects, setListProjects] = useState();
  const [listCommunities, setListCommunities] = useState();
  useEffect(() => {
    // temp fix to redo those functions when you go from a user page to another
    if (user.id) {
      setListProjects(undefined);
      setListCommunities(undefined);
      loadProjects(user.id);
      loadGroups(user.id);
    }
  }, [user]);
  const { userData } = useUserData();
  const { formatMessage } = useIntl();
  const router = useRouter();
  const api = useApi();
  const theme = useTheme();
  const [mutualCounts, setMutualCounts] = useState();
  const { data: dataExternalLink } = useGet(`/api/users/${user?.id}/links`);
  useEffect(() => {
    userData &&
      api.get(`/api/users/${user.id}/mutual`).then((res) => {
        setMutualCounts(res.data);
      });
  }, []);
  useEffect(() => {
    const urlSuccessParam = router.query.success;
    setTimeout(() => {
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
      stickyTabNav('isEdit', router); // make the tab navigation bar sticky on top when we reach its scroll position
      if (urlSuccessParam === '1') {
        // if url success param is 1, show "saved changes" success alert
        $('#editSuccess').show(); // display success message
        setTimeout(() => {
          $('#editSuccess').hide(300);
        }, 2000); // hide it after 2sec
      }
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  const loadProjects = (userId) => {
    api
      .get(`/api/users/${userId}/objects/projects`)
      .then((res) => {
        setListProjects(res.data);
      })
      .catch((err) => console.error(`Couldn't GET projects of userId=${userId}`, err));
  };
  const loadGroups = (userId) => {
    api.get(`/api/users/${userId}/objects/communities`).then((res) => {
      setListCommunities(res.data);
    });
  };
  // const loadSavedObjects = () => {
  //   api.get('/api/users/saved_objects').then((res) => {
  //     setListSavedObjects(res.data);
  //   });
  // };

  const bioWithLinks = user.bio ? linkify(user.bio) : '';

  // make different tab active depending if user is member or not
  // TODO Pass active as prop to the styled component
  let newsTabClasses;
  let aboutTabClasses;
  let newsPaneClasses;
  let aboutPaneClasses;
  if (user && userData) {
    if (!user.has_followed && user.id !== userData.id) {
      newsTabClasses = 'nav-item nav-link';
      aboutTabClasses = 'nav-item nav-link active';
      newsPaneClasses = 'tab-pane';
      aboutPaneClasses = 'tab-pane active';
    } else {
      newsTabClasses = 'nav-item nav-link active';
      aboutTabClasses = 'nav-item nav-link';
      newsPaneClasses = 'tab-pane active';
      aboutPaneClasses = 'tab-pane';
    }
  } else {
    newsTabClasses = 'nav-item nav-link';
    aboutTabClasses = 'nav-item nav-link active';
    newsPaneClasses = 'tab-pane';
    aboutPaneClasses = 'tab-pane active';
  }

  return (
    <Layout
      title={`${user.first_name} ${user.last_name} | JOGL`}
      desc={user.bio ? user.bio : user.short_bio}
      img={user.logo_url}
    >
      <div className="userProfile">
        <Loading active={false}>
          <div className="userHeader justify-content-center container-fluid">
            <UserHeader user={user} />
          </div>
          <nav className="nav nav-tabs container-fluid">
            <a className={newsTabClasses} href="#news" data-toggle="tab">
              <FormattedMessage id="user.profile.tab.feed" defaultMessage="Feed" />
            </a>
            <a className={aboutTabClasses} href="#about" data-toggle="tab">
              <FormattedMessage id="user.profile.tab.about" defaultMessage="About" />
            </a>
            <a
              className="nav-item nav-link"
              href="#collections"
              data-toggle="tab"
              onClick={() => loadProjects(user.id) + loadGroups(user.id)}
            >
              <FormattedMessage id="user.profile.tab.collections" defaultMessage="Collection" />
            </a>
            {/* <a className="nav-item nav-link" href="#saved" data-toggle="tab" onClick={() => loadSavedObjects()}>
              <FormattedMessage id="user.profile.tab.saved" defaultMessage="My saved objects" />
            </a> */}
          </nav>
          <div className="tabContainer">
            <div className="tab-content justify-content-center container-fluid">
              <div className={newsPaneClasses} id="news">
                {user.feed_id && (
                  <Feed
                    feedId={user.feed_id}
                    displayCreate={
                      // show post creation box only to the user
                      userData && user.id === userData.id
                    }
                    isAdmin={user.is_admin}
                  />
                )}
              </div>

              <div className={aboutPaneClasses} id="about">
                <Box width={['100%', undefined, undefined, '67%']} margin="auto">
                  <InfoDefaultComponent
                    title={formatMessage({ id: 'user.profile.bio', defaultMessage: 'Bio' })}
                    content={bioWithLinks}
                    containsHtml
                  />
                  <InfoDefaultComponent
                    title={formatMessage({ id: 'user.profile.affiliation', defaultMessage: 'Affiliation' })}
                    content={user.affiliation}
                  />
                  {user.category && user.category !== 'default' && (
                    <InfoDefaultComponent
                      title={formatMessage({ id: 'user.profile.category', defaultMessage: 'Category' })}
                      content={formatMessage({
                        id: `user.profile.edit.select.${user.category}`,
                        defaultMessage: user.category,
                      })}
                    />
                  )}
                  {user.skills.length !== 0 && (
                    <Box className="infoSkills" mb={4}>
                      <Box fontWeight="bold">
                        {formatMessage({ id: 'user.profile.skills', defaultMessage: 'Skills' })}
                      </Box>
                      <Chips
                        data={user.skills.map((skill) => ({
                          title: skill,
                          as: `/search/members/?refinementList[skills][0]=${skill}`,
                          href: `/search/[active-index]/?refinementList[skills][0]=${skill}`,
                        }))}
                        color={theme.colors.primary}
                      />
                    </Box>
                  )}
                  {user.ressources.length !== 0 && (
                    <Box className="infoResources" mb={4}>
                      <Box fontWeight="bold">
                        {formatMessage({ id: 'user.profile.resources', defaultMessage: 'Resources' })}
                      </Box>
                      <Chips
                        data={user.ressources.map((resource) => ({
                          title: resource,
                          as: `/search/members/?refinementList[ressources][0]=${resource}`,
                          href: `/search/[active-index]/?refinementList[ressources][0]=${resource}`,
                        }))}
                        color={theme.colors.pink}
                      />
                    </Box>
                  )}
                  <InfoInterestsComponent
                    title={formatMessage({ id: 'user.profile.interests', defaultMessage: 'Interests' })}
                    content={user.interests}
                  />
                  <InfoAddressComponent
                    title={formatMessage({ id: 'user.profile.address', defaultMessage: 'Address' })}
                    address={user.address}
                    city={user.city}
                    country={user.country}
                  />
                  {userData?.id !== user.id && (mutualCounts?.length > 0 || user.projects_count > 0) && (
                    <Box pb={6}>
                      <TitleInfo
                        title={formatMessage({ id: 'user.info.other', defaultMessage: 'Other information' })}
                      />
                      {user.projects_count > 0 && (
                        <Box pt={2}>{`${formatMessage({
                          id: 'user.info.currentlyOn',
                          defaultMessage: 'Currently on',
                        })} ${user.projects_count} ${textWithPlural('project', user.projects_count)}`}</Box>
                      )}
                      {mutualCounts?.length > 0 && (
                        <Box pt={2}>{`${mutualCounts.length} ${textWithPlural(
                          'mutualConnection',
                          mutualCounts.length
                        )}`}</Box>
                      )}
                    </Box>
                  )}
                  {dataExternalLink && dataExternalLink?.length !== 0 && (
                    <Box>
                      <TitleInfo
                        title={formatMessage({ id: 'general.externalLink.title', defaultMessage: 'External links' })}
                      />
                      <Grid display="grid" gridTemplateColumns="repeat(auto-fill, minmax(55px, 1fr))" mt={2}>
                        {[...dataExternalLink].map((link, i) => (
                          <ExternalLinkIcon alignSelf="center" key={i} pb={3}>
                            <a href={link.url} target="_blank">
                              <img width="45px" src={link.icon_url} />
                            </a>
                          </ExternalLinkIcon>
                        ))}
                      </Grid>
                    </Box>
                  )}
                </Box>
              </div>

              <div className="tab-pane" id="collections">
                <h3>
                  <FormattedMessage id="user.profile.tab.projects" defaultMessage="Projects" />
                </h3>
                <ProjectList listProjects={listProjects} />
                <div className="communityList" style={{ marginTop: '2.6rem' }}>
                  <h3>
                    <FormattedMessage id="user.profile.tab.communities" defaultMessage="Communities" />
                  </h3>
                  <CommunityList listCommunities={listCommunities} />
                </div>
              </div>
            </div>
          </div>
          <Alert
            type="success"
            id="editSuccess"
            message={
              <FormattedMessage
                id="general.editSuccessMsg"
                defaultMessage="The changes have been saved successfully."
              />
            }
          />
        </Loading>
      </div>
    </Layout>
  );
}

const ExternalLinkIcon = styled(Box)`
  img:hover {
    opacity: 0.8;
  }
`;

UserProfile.getInitialProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/users/${ctx.query.id}`)
    .catch((err) => console.error(`Couldn't GET user with id=${ctx.query.id}`, err));
  if (res) {
    return { user: res.data };
  }
  isomorphicRedirect(ctx, '/search/[active-index]', '/search/members');
  return {};
};
