/* eslint-disable camelcase */
import { useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Link from 'next/link';
import Router from 'next/router';
import nextCookie from 'next-cookies';
import { toAlphaNum } from '~/components/Tools/Nickname';
/** * Form objects ** */
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormTextAreaComponent from '~/components/Tools/Forms/FormTextAreaComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from '~/components/Tools/Forms/FormResourcesComponent';
import FormToggleComponent from '~/components/Tools/Forms/FormToggleComponent';
/** * Validators ** */
import FormValidator from '~/components/Tools/Forms/FormValidator';
import userProfileFormRules from '~/components/User/userProfileFormRules.json';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import Box from '~/components/Box';
import Button from '~/components/primitives/Button';
import { useModal } from '~/contexts/modalContext';
// import { Select as SelectVirtualized } from 'react-select-virtualized';
import Select, { createFilter } from 'react-select';
// import Select from 'react-select';
import { FixedSizeList as List } from 'react-window';
// import styled from '~/utils/styled';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import TitleInfo from '~/components/Tools/TitleInfo';

// import "./UserProfileEdit.scss";
const CountriesCitiesData = require('public/data/countries-cities');

const UserProfileEdit = ({ user: userProp }) => {
  const validator = new FormValidator(userProfileFormRules);
  const [user, setUser] = useState(userProp);
  const [userUpdated, setUserUpdated] = useState(false);
  const [stateValidation, setStateValidation] = useState({});
  const [isSending, setIsSending] = useState(false);
  const { showModal } = useModal();
  const api = useApi();
  const { formatMessage } = useIntl();

  // check if current user country is in the official array of countries (had to do this for when users could type their country with a text input, and they could type it differently..)
  const countryNotInList = !Object.keys(CountriesCitiesData.list).includes(user.country);

  const countriesList = Object.keys(CountriesCitiesData.list) // an array of all the countries from the list
    .sort((a, b) => a.localeCompare(b))
    .map((content) => {
      return { value: content, label: content };
    });
  const citiesList =
    user.country &&
    !countryNotInList &&
    CountriesCitiesData.list[user.country] // to access the country array containing all its cities
      .sort((a, b) => a.localeCompare(b))
      .map((content) => {
        return { value: content, label: content };
      });

  const MenuList = ({ children, maxHeight }) => {
    return (
      <List height={maxHeight} itemCount={children.length} itemSize={30}>
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  };

  // const SelectLargeData = styled(SelectVirtualized)`
  //   width: 100%;
  //   .fast-option-selected {
  //     color: white;
  //   }
  // `;

  if (userUpdated) {
    Router.push(
      { pathname: '/user/[id]/[[...index]]', query: { success: 1 } },
      `/user/${user.id}/${user.nickname}`
    ).then(() => window.scrollTo(0, 0)); // force scroll to top of page @TODO might not need anymore once this issue will be fixed: https://github.com/vercel/next.js/issues/15206
  }

  const handleChange = (key, content) => {
    setUser((prevUser) => ({ ...prevUser, [key]: content }));
    /* Validators start */
    const state = {};
    if (key === 'nickname') {
      state[key] = toAlphaNum(content);
    } else state[key] = content;
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };

  const handleCountryChange = (key, content) => {
    if (content.action === 'set-value' || content.action === 'select-option')
      setUser((prevUser) => ({ ...prevUser, ['country']: key.value }));
    if (content.action === 'clear') setUser((prevUser) => ({ ...prevUser, ['country']: '' }));
    /* Validators start */
    const state = {};
    state['country'] = key ? key.value : undefined;
    const validation = validator.validate(state);
    if (validation['country'] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_country`] = validation['country'];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };
  const handleCityChange = (key, content) => {
    if (content.action === 'set-value' || content.action === 'select-option')
      setUser((prevUser) => ({ ...prevUser, ['city']: key.value }));
    if (content.action === 'clear') setUser((prevUser) => ({ ...prevUser, ['city']: '' }));
  };

  const handleSubmit = () => {
    setIsSending(true);
    /* Validators control before submit */
    const validation = validator.validate(user);
    if (validation.isValid) {
      api.patch(`/api/users/${user.id}`, { user }).then(() => {
        setUserUpdated(true);
        setIsSending(false);
      });
    } else {
      setIsSending(false);
      let firstError = true;
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        // map through all the object of validation
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 100; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };
  const {
    valid_first_name,
    valid_last_name,
    valid_nickname,
    valid_category,
    valid_country,
    valid_affiliation,
    valid_interests,
    valid_ressources,
    valid_skills,
    valid_short_bio,
  } = stateValidation || '';

  const countriesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
    control: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      border: valid_country
        ? valid_country.isInvalid
          ? '1px solid #dc3545'
          : !valid_country.isInvalid && '1px solid #28a745'
        : '1px solid lightgrey',
    }),
  };
  const citiesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
  };

  return (
    <Layout title={`${user?.first_name} ${user?.last_name} | JOGL`} desc={user?.bio} img={user?.logo_url}>
      {user ? (
        <div className="userProfileEdit">
          <div className="container-fluid justify-content-center">
            <h1>
              <FormattedMessage id="user.profile.edit.title" defaultMessage="Edit my Profile" />
            </h1>
            <form>
              <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                <FormDefaultComponent
                  id="first_name"
                  placeholder={formatMessage({
                    id: 'user.profile.firstname.placeholder',
                    defaultMessage: 'John',
                  })}
                  title={formatMessage({ id: 'user.profile.firstname', defaultMessage: 'First name' })}
                  content={user.first_name}
                  onChange={handleChange}
                  errorCodeMessage={valid_first_name ? valid_first_name.message : ''}
                  isValid={valid_first_name ? !valid_first_name.isInvalid : undefined}
                  mandatory
                />
                <FormDefaultComponent
                  id="last_name"
                  placeholder={formatMessage({ id: 'user.profile.lastname.placeholder', defaultMessage: 'Doe' })}
                  title={formatMessage({ id: 'user.profile.lastname', defaultMessage: 'Last name' })}
                  content={user.last_name}
                  onChange={handleChange}
                  errorCodeMessage={valid_last_name ? valid_last_name.message : ''}
                  isValid={valid_last_name ? !valid_last_name.isInvalid : undefined}
                  mandatory
                />
                <FormDefaultComponent
                  id="nickname"
                  placeholder={formatMessage({
                    id: 'user.profile.nickname.placeholder',
                    defaultMessage: 'johndoe',
                  })}
                  title={formatMessage({ id: 'user.profile.nickname', defaultMessage: 'Username' })}
                  content={user.nickname}
                  onChange={handleChange}
                  prepend="@"
                  mandatory
                  errorCodeMessage={valid_nickname ? valid_nickname.message : ''}
                  isValid={valid_nickname ? !valid_nickname.isInvalid : undefined}
                  pattern={/[A-Za-z0-9]/g}
                />
              </Box>
              <FormDefaultComponent
                id="short_bio"
                placeholder={formatMessage({
                  id: 'user.profile.bioShort.placeholder',
                  defaultMessage: 'Describe yourself in a few words',
                })}
                title={formatMessage({ id: 'user.profile.bioShort', defaultMessage: 'Short bio' })}
                content={user.short_bio}
                maxChar={140}
                mandatory
                errorCodeMessage={valid_short_bio ? valid_short_bio.message : ''}
                isValid={valid_short_bio ? !valid_short_bio.isInvalid : undefined}
                onChange={handleChange}
              />
              <FormTextAreaComponent
                id="bio"
                placeholder={formatMessage({
                  id: 'user.profile.bio.placeholder',
                  defaultMessage: 'Describe yourself in detail',
                })}
                title={formatMessage({ id: 'user.profile.bio', defaultMessage: 'Bio' })}
                content={user.bio}
                rows={5}
                onChange={handleChange}
              />
              <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                <FormDropdownComponent
                  id="category"
                  title={formatMessage({ id: 'user.profile.category', defaultMessage: 'Category' })}
                  content={user.category}
                  mandatory
                  errorCodeMessage={valid_category ? valid_category.message : ''}
                  // prettier-ignore
                  options={['fulltime_worker', "parttime_worker", "fulltime_student", "parttime_student",
                  "freelance", "between_jobs", "retired", "student_looking_internship"]}
                  onChange={handleChange}
                />
                <FormDefaultComponent
                  id="affiliation"
                  placeholder={formatMessage({
                    id: 'user.profile.affiliation.placeholder',
                    defaultMessage: 'Your company, startup, association, NGO...',
                  })}
                  title={formatMessage({ id: 'user.profile.affiliation', defaultMessage: 'Affiliation' })}
                  content={user.affiliation}
                  mandatory
                  errorCodeMessage={valid_affiliation ? valid_affiliation.message : ''}
                  isValid={valid_affiliation ? !valid_affiliation.isInvalid : undefined}
                  onChange={handleChange}
                />
              </Box>
              <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                {/* countries dropdown */}
                <div className="formDropdown">
                  <TitleInfo mandatory title={formatMessage({ id: 'general.country', defaultMessage: 'Country' })} />
                  <div className="content">
                    <Select
                      name="country"
                      id="country"
                      defaultValue={user.country && { label: user.country, value: user.country }}
                      options={countriesList}
                      placeholder={formatMessage({
                        id: 'general.country.placeholder',
                        defaultMessage: 'Select a country',
                      })}
                      filterOption={createFilter({ ignoreAccents: false })} // this line greatly improves performance
                      components={{ MenuList }}
                      noOptionsMessage={() => null}
                      onChange={handleCountryChange}
                      styles={countriesSelectStyles}
                      isClearable
                    />
                    {user.country &&
                    countryNotInList && ( // ask user to re-select their country if it's not part of the new countries list we have (wrong spelling for ex)
                        <Box color="#dc3545">
                          {formatMessage({
                            id: 'general.country.notInList',
                            defaultMessage: 'Please re-select your country',
                          })}
                        </Box>
                      )}
                    {valid_country?.isInvalid && (
                      <Box color="#dc3545">
                        <FormattedMessage id={valid_country?.message || ''} defaultMessage="Value is not valid" />
                      </Box>
                    )}
                  </div>
                </div>
                {/* cities dropdown */}
                {user.country && !countryNotInList && (
                  <div className="formDropdown">
                    <TitleInfo title={formatMessage({ id: 'general.city', defaultMessage: 'City' })} />
                    <div className="content">
                      {/* <SelectLargeData
                        name="city"
                        defaultValue={user.city && { label: user.city, value: user.city }}
                        options={citiesList}
                        placeholder={formatMessage({
                          id: 'general.city.placeholder',
                          defaultMessage: 'Select a city',
                        })}
                        filterOption={createFilter({ ignoreAccents: true })}
                        noOptionsMessage={() => null}
                        onChange={handleCityChange}
                      /> */}
                      <Select
                        name="city"
                        defaultValue={user.city && { label: user.city, value: user.city }}
                        options={citiesList}
                        placeholder={formatMessage({
                          id: 'general.city.placeholder',
                          defaultMessage: 'Select a city',
                        })}
                        filterOption={createFilter({ ignoreAccents: citiesList.length > 2150 ? false : true })} // ignore accents for countries that have over 2150 cities, because it greatly improves performance!
                        components={{ MenuList }}
                        noOptionsMessage={() => null}
                        onChange={handleCityChange}
                        styles={citiesSelectStyles}
                        isClearable
                      />
                    </div>
                  </div>
                )}
              </Box>
              <Button
                mt={3}
                mb={2}
                onClick={() => {
                  showModal({
                    children: <ManageExternalLink itemType="users" itemId={user.id} showTitle={false} />,
                    title: 'Social networks accounts',
                    titleId: 'general.externalLink.addView',
                    maxWidth: '70rem',
                    allowOverflow: true,
                  });
                }}
                type="button"
              >
                <FormattedMessage id="general.externalLink.addView" defaultMessage="Social networks accounts" />
              </Button>
              <FormImgComponent
                itemId={user.id}
                fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                maxSizeFile={4194304}
                itemType="users"
                type="avatar"
                id="logo_url"
                title={formatMessage({ id: 'user.profile.logo_url', defaultMessage: 'Profile image' })}
                imageUrl={user.logo_url}
                defaultImg="/images/default/default-user.png"
                onChange={handleChange}
              />
              <FormInterestsComponent
                id="interests"
                title={formatMessage({ id: 'user.profile.interests', defaultMessage: 'Interested in' })}
                content={user.interests}
                mandatory
                errorCodeMessage={valid_interests ? valid_interests.message : ''}
                isValid={valid_interests ? !valid_interests.isInvalid : undefined}
                onChange={handleChange}
              />
              <FormSkillsComponent
                id="skills"
                type="user"
                placeholder={formatMessage({
                  id: 'general.skills.placeholder',
                  defaultMessage: 'Big data, Web Development, Open Science...',
                })}
                title={formatMessage({ id: 'user.profile.skills', defaultMessage: 'Skills' })}
                content={user.skills}
                mandatory
                errorCodeMessage={valid_skills ? valid_skills.message : ''}
                isValid={valid_skills ? !valid_skills.isInvalid : undefined}
                onChange={handleChange}
              />
              <FormResourcesComponent
                id="ressources"
                type="user"
                placeholder={formatMessage({
                  id: 'general.resources.placeholder',
                  defaultMessage: '3D printers, Biolab, Makerspace...',
                })}
                title={formatMessage({ id: 'user.profile.resources', defaultMessage: 'Resources' })}
                content={user.ressources}
                mandatory={false}
                errorCodeMessage={valid_ressources ? valid_ressources.message : ''}
                isValid={valid_ressources ? !valid_ressources.isInvalid : undefined}
                onChange={handleChange}
              />
              {/* <FormDefaultComponent
                id="age"
                placeholder={formatMessage({ id: 'user.profile.age.placeholder', defaultMessage: 'Your age' })}
                title={formatMessage({ id: 'user.profile.age', defaultMessage: 'Age' })}
                content={user.age}
                onChange={handleChange}
                notPublic
              /> */}
              <FormToggleComponent
                id="can_contact"
                title={formatMessage({
                  id: 'user.profile.canContact',
                  defaultMessage: 'Can other users contact me?',
                })}
                choice1={<FormattedMessage id="general.no" defaultMessage="No" />}
                choice2={<FormattedMessage id="general.yes" defaultMessage="Yes" />}
                isChecked={user.can_contact || user.can_contact === null}
                onChange={handleChange}
              />
              <FormToggleComponent
                id="mail_newsletter"
                title={formatMessage({
                  id: 'signUp.mail_newsletter',
                  defaultMessage: 'I would like to subscribe to the JOGL monthly newsletter',
                })}
                choice1={<FormattedMessage id="general.no" defaultMessage="No" />}
                choice2={<FormattedMessage id="general.yes" defaultMessage="Yes" />}
                isChecked={user.mail_newsletter}
                onChange={handleChange}
              />
              {/* <FormToggleComponent
                id="mail_weekly"
                title={formatMessage({
                  id: 'signUp.mail_weekly',
                  defaultMessage: 'Do you want to receive weekly updates of JOGL items you follow?',
                })}
                choice1={<FormattedMessage id="general.no" defaultMessage="No" />}
                choice2={<FormattedMessage id="general.yes" defaultMessage="Yes" />}
                isChecked={user.mail_weekly}
                onChange={handleChange}
              /> */}
              <div className="buttons">
                <Link href="/user/[id]/[[...index]]" as={`/user/${user.id}/${user.nickname}`}>
                  <a>
                    <button type="button" className="btn btn-outline-primary">
                      <FormattedMessage id="user.profile.edit.back" defaultMessage="Back" />
                    </button>
                  </a>
                </Link>
                <button
                  className="btn btn-primary"
                  style={{ marginRight: '10px' }}
                  type="button"
                  disabled={isSending}
                  onClick={handleSubmit}
                >
                  {isSending && (
                    <>
                      <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="user.profile.edit.submit" defaultMessage="Update" />
                </button>
              </div>
            </form>
          </div>
        </div>
      ) : (
        <div className="errorMessage text-center">Unable to load component</div>
      )}
    </Layout>
  );
};
UserProfileEdit.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const { userId } = nextCookie(ctx);
  const res = await api
    .get(`/api/users/${query.id}`)
    .catch((err) => console.error(`Could not fetch user with id=${query.id}`, err));
  // Check if it got the user and if user is the connected user, else redirect to user page
  if (query.id === userId) return { user: res.data };
  isomorphicRedirect(ctx, '/user/[id]', `/user/${query.id}`);
};

export default UserProfileEdit;
