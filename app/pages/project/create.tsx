import { useState, useContext } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Router from 'next/router';
import ProjectForm from '~/components/Project/ProjectForm';
import Layout from '~/components/Layout';
import { UserContext } from '~/contexts/UserProvider';
import { useApi } from '~/contexts/apiContext';
import { NextPage } from 'next';
import ReactGA from 'react-ga';

interface Props {}
const ProjectCreate: NextPage<Props> = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const { formatMessage } = useIntl();
  const [newProject, setNewProject] = useState({
    title: '',
    short_title: '',
    logo_url: '',
    description: '',
    short_description: '',
    creator_id: Number(userContext.credentials.userId),
    status: 'draft',
    interests: [],
    skills: [],
    banner_url: '',
    is_private: false, // force all new projects to be private (TEMP @TOFIX)
  });
  const [sending, setSending] = useState(false);

  const handleChange = (key, content) => {
    setNewProject((prevProject) => ({ ...prevProject, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .post('/api/projects/', { project: newProject })
      .then((res) => {
        // record event to Google Analytics
        ReactGA.event({ category: 'Project', action: 'create', label: `${res.data.id}` /* proj id */ });
        // go to the created project edition page
        Router.push('/project/[id]/edit', `/project/${res.data.id}/edit`);
      })
      .catch((err) => {
        console.error("Couldn't post new project", err);
        setSending(false);
      });
  };
  return (
    <Layout title={`${formatMessage({ id: 'project.create.title', defaultMessage: 'Create a new project' })} | JOGL`}>
      <div className="projectCreate container-fluid">
        <h1>
          <FormattedMessage id="project.create.title" defaultMessage="Create a new project" />
        </h1>
        <ProjectForm
          mode="create"
          project={newProject}
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          sending={sending}
        />
      </div>
    </Layout>
  );
};

export default ProjectCreate;
