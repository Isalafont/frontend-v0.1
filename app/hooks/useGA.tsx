import { Router } from 'next/router';
import { useEffect } from 'react';
import { initGA, logPageView } from '../utils/analytics';

export default function useGA(GOOGLE_ANALYTICS_ID) {
  useEffect(() => {
    // google analytics
    if (GOOGLE_ANALYTICS_ID) {
      initGA(GOOGLE_ANALYTICS_ID);
      logPageView();
      Router.events.on('routeChangeComplete', logPageView);
    }
  }, [GOOGLE_ANALYTICS_ID]);
}
