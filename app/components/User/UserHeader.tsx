import React, { useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Link from 'next/link';
// import $ from "jquery";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useUserData from '~/hooks/useUserData';
import BtnFollow from '../Tools/BtnFollow';
import ListFollowers from '../Tools/ListFollowers';
import UserShowObjects from '~/components/User/UserShowObjects';
import { useModal } from '~/contexts/modalContext';
import { ContactForm } from '../Tools/ContactForm';
import Button from '../primitives/Button';
import useGet from '~/hooks/useGet';
import { textWithPlural } from '~/utils/managePlurals';
import Box from '../Box';
import ReactGA from 'react-ga';
import Chips from '../Chip/Chips';
import { useTheme } from 'emotion-theming';
// import "./UserHeader.scss";

const UserHeader = ({ user }) => {
  useEffect(() => {
    $('.moreSkills').click(() => {
      // when click on the "+..." skills
      $('a[href="#about"]').click(); // force click on about tab
      const element = document.querySelector('.tabContainer .infoSkills'); // get skills section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to section
    });
    $('.moreResources').click(() => {
      // when click on the "+..." resources
      $('a[href="#about"]').click(); // force click on about tab
      const element = document.querySelector('.tabContainer .infoResources'); // get resources section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to section
    });
  });

  const { formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const { userData } = useUserData();
  const theme = useTheme();
  // prettier-ignore
  let {
    id, logo_url, skills, ressources, nickname, first_name, last_name, bio, short_bio,
    follower_count, following_count, can_contact, has_followed
  } = user;
  if (!logo_url) {
    logo_url = '/images/default/default-user.png';
  }
  const logoStyle = {
    backgroundImage: `url(${logo_url})`,
  };

  const openFollowersModal = (e) => {
    follower_count && // open modal only if user has followers
    ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={id} itemType="users" />,
        title: 'Followers',
        titleId: 'entity.tab.followers',
        maxWidth: '70rem',
      });
  };

  const openFollowingModal = (e) => {
    following_count && // open modal only if user is following objects
    ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <FollowingModal userId={id} />,
        title: 'Following',
        titleId: 'user.profile.tab.following',
        maxWidth: '70rem',
      });
  };

  const showUserProfilePicture = () => {
    showModal({
      children: (
        <Box>
          <img src={logo_url} style={{ objectFit: 'contain' }} />
        </Box>
      ),
      showCloseButton: true,
      maxWidth: '30rem',
    });
  };

  const showSkillsAndResourcesChips = () => (
    <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
      {skills.length !== 0 && (
        <Box>
          <Box color={theme.colors.secondary}>
            {formatMessage({ id: 'user.profile.skills', defaultMessage: 'Skills' })}
          </Box>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              as: `/search/members/?refinementList[skills][0]=${skill}`,
              href: `/search/[active-index]/?refinementList[skills][0]=${skill}`,
            }))}
            overflowLink="userSkill"
            color={theme.colors.primary}
            showCount={4}
          />
        </Box>
      )}
      {ressources.length !== 0 && (
        <Box>
          <Box color={theme.colors.secondary}>
            {formatMessage({ id: 'user.profile.resources', defaultMessage: 'Resources' })}
          </Box>
          <Chips
            data={ressources.map((resource) => ({
              title: resource,
              as: `/search/members/?refinementList[ressources][0]=${resource}`,
              href: `/search/[active-index]/?refinementList[ressources][0]=${resource}`,
            }))}
            overflowLink="resourceSkill"
            color={theme.colors.pink}
            showCount={4}
          />
        </Box>
      )}
    </Box>
  );

  return (
    <div className="userHeader--top row">
      <div className="col-lg-2 col-12 d-none d-lg-block">
        <div className="userImg" style={logoStyle} onClick={showUserProfilePicture} />
      </div>

      <div className="col-lg-10 col-12 userInfos">
        <div className="infoContainer">
          <div className="userSmallImg">
            <div style={logoStyle} onClick={showUserProfilePicture} />
          </div>
          <div className="firstRow">
            <div className="nameInfos">
              <h1 className="title">{`${first_name} ${last_name}`}</h1>
              <p className="nickname">{`@${nickname}`}</p>
            </div>
            <Box display={['none', 'flex']}>{showSkillsAndResourcesChips()}</Box>
          </div>
        </div>
        <div className="userStats">
          {/* show follow button if user is not connected, or if he is connected but not the user viewed */}
          {(!userData || (userData && userData.id !== id)) && (
            <BtnFollow followState={has_followed} itemType="users" itemId={id} displayButton />
          )}
          {/* show contact button if user is connected, that he's not the viewed user, and that viewed user wants to be contacted */}
          {userData && userData.id !== id && can_contact !== false && (
            <Button
              btnType="secondary"
              onClick={() => {
                ReactGA.modalview('/send-message');
                showModal({
                  children: <ContactForm itemId={id} closeModal={() => setIsOpen(false)} />,
                  title: 'Send message to {userFullName}',
                  titleId: 'user.contactModal.title',
                  values: { userFullName: first_name + ' ' + last_name },
                });
              }}
            >
              {formatMessage({ id: 'user.btn.contact', defaultMessage: 'Contact' })}
            </Button>
          )}
          <div>
            <span className="text" tabIndex={0} onClick={openFollowersModal} onKeyUp={openFollowersModal}>
              <strong>{follower_count}</strong>&nbsp;{textWithPlural('follower', follower_count)}
            </span>
            <span className="text" tabIndex={0} onClick={openFollowingModal} onKeyUp={openFollowingModal}>
              <strong>{following_count || 0}</strong>&nbsp;{textWithPlural('following', following_count)}
            </span>
          </div>
        </div>
        <p className="about">{short_bio || bio}</p>

        <Box display={['flex', 'none']}>{showSkillsAndResourcesChips()}</Box>
        {/* display projects counts and mutual connections (if you are connected and not the user */}
        {/* {userData?.id !== id && (mutualCounts?.length > 0 || projects_count > 1) && (
          <Box row spaceX={4}>
            {projects_count > 1 && (
              <>
                <Box>{`*Currently on ${projects_count} projects`}</Box>
                {mutualCounts?.length > 0 && <Box>•</Box>}
              </>
            )}
            {mutualCounts?.length > 0 && <Box>{`${mutualCounts.length} *mutual connections`}</Box>}
          </Box>
        )} */}
      </div>

      {userData && userData.id === id && (
        <Box className="col-12 userActions" pt={[4, undefined, undefined, 0]}>
          <Link href="/user/[id]/edit" as={`/user/${id}/edit`}>
            <a>
              <FontAwesomeIcon icon="edit" />
              <FormattedMessage id="user.profile.edit.btnEdit" defaultMessage="Edit Profile" />
            </a>
          </Link>
        </Box>
      )}
    </div>
  );
};

const FollowingModal = ({ userId }) => {
  const { data: followings } = useGet(`/api/users/${userId}/following`);
  return <UserShowObjects list={followings} />;
};
const SavedObjectsModal = () => {
  const { data: savedObjects } = useGet(`/api/users/saved_objects`);
  return <UserShowObjects list={savedObjects} type="saved" />;
};

export default UserHeader;
