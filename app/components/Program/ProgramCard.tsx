/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import { useIntl } from 'react-intl';
import { LayoutProps } from 'styled-system';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { textWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';

interface Props {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  clapsCount?: number;
  membersCount?: number;
  projectsCount?: number;
  needsCount?: number;
  has_saved: boolean;
  banner_url: string;
  width?: LayoutProps['width'];
  source?: 'algolia' | 'api';
  cardFormat?: string;
}
const ProgramCard = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  clapsCount,
  membersCount,
  projectsCount,
  needsCount,
  has_saved,
  banner_url = '/images/default/default-program.jpg',
  width,
  source,
  cardFormat,
}: Props) => {
  const { locale } = useIntl();
  const programUrl = { href: '/program/[short_title]', as: `/program/${short_title}` };
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '4xl';
  return (
    <Card imgUrl={banner_url} imgLinkObject={programUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={programUrl.href} as={programUrl.as} passHref>
          <Title pr={2}>
            <H2 fontSize={TitleFontSize}>{locale === 'fr' && title_fr ? title_fr : title}</H2>
          </Title>
        </Link>
        <BtnSave itemType="programs" itemId={id} saveState={has_saved} source={source} />
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <OverflowDesc flex="1">
        {locale === 'fr' && short_description_fr ? short_description_fr : short_description}
      </OverflowDesc>
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2}>
          <CardData value={membersCount} title={textWithPlural('member', membersCount)} />
          <CardData value={needsCount} title={textWithPlural('need', needsCount)} />
          <CardData value={projectsCount} title={textWithPlural('project', projectsCount)} />
          {/* <CardData value={clapsCount} title={textWithPlural('clap', clapsCount)} /> */}
        </Box>
      )}
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

const OverflowDesc = styled(Box)`
  overflow: hidden;
  color: #343a40;
  display: -webkit-box;
  -webkit-line-clamp: 6;
  -webkit-box-orient: vertical;
`;

export default ProgramCard;
