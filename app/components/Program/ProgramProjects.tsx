import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { layout } from 'styled-system';
import useChallenges from '~/hooks/useChallenges';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Project } from '~/types';
import styled from '~/utils/styled';
import Box from '../Box';
import Filters from '../Filters';
import Grid from '../Grid';
import A from '../primitives/A';
import P from '../primitives/P';
import ProjectCard from '../Project/ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

const ProgramProjects = ({ programId }) => {
  const [projectsEndpoints, setProjectsEndpoint] = useState(`/api/programs/${programId}/projects`);
  const { dataChallenges, challengesError } = useChallenges('programs', programId);
  const { data: dataProjects, error: projectsError } = useGet<{ projects: Project[] }>(projectsEndpoints);
  const [selectedChallengeFilterId, setSelectedChallengeFilterId] = useState(undefined);
  const { userData, userDataError } = useUserData();
  const onFilterChange = (e) => {
    const id = e.target.name;
    if (id) {
      setSelectedChallengeFilterId(Number(id));
      setProjectsEndpoint(`/api/challenges/${id}/projects`);
    } else {
      setSelectedChallengeFilterId(undefined);
      setProjectsEndpoint(`/api/programs/${programId}/projects`);
    }
  };

  const OverflowGradient = styled.div`
    ${layout};
    width: 3rem;
    height: 100%;
    position: absolute;
    right: 0;
    /* background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 134.37%); */
    background: ${(p) =>
      `linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`};
  `;
  return (
    <div>
      <Box>
        <P>
          <FormattedMessage
            id="program.projects.text"
            defaultMessage="To participate to this program, participate to as many of its projects. Check out the projects already submitted, contribute to them or create your own!"
          />
        </P>
        {!userData && ( // if user is not connected
          <A href="/signin" as="/signin">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toJoinProject}'}
              values={{
                toJoinProject: <FormattedMessage id="program.signinCta.project" defaultMessage="to join a project" />,
              }}
            />
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel={{ id: 'challenge.list.all.title', defaultMessage: 'All challenges' }}
            content={dataChallenges?.map(({ title, id }) => ({
              title,
              id,
            }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!challengesError}
            errorMessage="Could not get challenges filters"
            selectedId={selectedChallengeFilterId}
          />
        </Box>
        {!dataProjects ? (
          <Loading />
        ) : dataProjects?.projects.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} py={4}>
            {dataProjects.projects
              // don't display pending projects
              .filter(({ challenge_status }) => challenge_status !== 'pending')
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  clapsCount={project.claps_count}
                  has_saved={project.has_saved}
                  banner_url={project.banner_url}
                />
              ))}
          </Grid>
        )}
      </Box>
    </div>
  );
};

export default ProgramProjects;
