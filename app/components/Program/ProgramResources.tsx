import React, { FC } from 'react';
import { useIntl } from 'react-intl';
import Box from '~/components/Box';
import H2 from '~/components/primitives/H2';
import useGet from '~/hooks/useGet';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';

interface Props {
  programId: number;
}
const ProgramResources: FC<Props> = ({ programId }) => {
  const { data: resourcesList } = useGet(`/api/programs/${programId}/resources`);
  const { formatMessage } = useIntl();
  return (
    <Box>
      <H2 px={[3, 4]}>{formatMessage({ id: 'program.resourcesTitle', defaultMessage: 'Resources' })}</H2>
      {!resourcesList ? (
        <Loading />
      ) : resourcesList?.length === 0 ? (
        ''
      ) : (
        <Box spaceY={7} pt={9}>
          {[...resourcesList].map((resource, i) => (
            <Box>
              <H2 px={[3, 4]} pb={2}>
                {resource.title}
              </H2>
              <Box bg="white" px={[3, 4]}>
                <InfoHtmlComponent title="" content={resource.content} />
              </Box>
            </Box>
          ))}
        </Box>
      )}
    </Box>
  );
};

export default ProgramResources;
