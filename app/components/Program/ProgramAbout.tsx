import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React, { FC } from 'react';
import { FormattedDate, useIntl } from 'react-intl';
import H2 from '~/components/primitives/H2';
import P from '~/components/primitives/P';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import styled from '~/utils/styled';
import Box from '../Box';
import Card from '../Card';
import Carousel from '../Carousel';
import ChallengeMiniCard from '../Challenge/ChallengeMiniCard';
import { ContactForm } from '../Tools/ContactForm';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import ReactGA from 'react-ga';

interface Props {
  programId: number;
  enablers: string;
  description: string;
  dataChallenges: [];
  launchDate: Date;
  status: string;
}

const ProgramAbout: FC<Props> = ({ programId, enablers, description, dataChallenges, launchDate, status }) => {
  const { formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const { data: dataBoards } = useGet(`/api/programs/${programId}/boards`);

  const BoardCard = styled(Card)`
    position: relative;
    h2:hover {
      cursor: pointer;
      text-decoration: underline;
    }
  `;

  const ViewMore = styled('span')`
    position: absolute;
    bottom: 0;
    right: 0;
    margin-bottom: 0;
    padding-left: 8px;
    background-color: white;
    font-size: 15px;
    cursor: pointer;
    text-transform: lowercase;

    &:hover {
      box-shadow: ${(p) => p.theme.colors.primary};
      text-decoration: underline;
    }
  `;

  const showBoardModal = (title, description, users) => {
    showModal({
      children: (
        <BoardMembersModal description={description} users={users} showModal={showModal} setIsOpen={setIsOpen} />
      ),
      title: title,
      maxWidth: '60rem',
    });
  };

  return (
    <>
      {/* program full description */}
      <InfoHtmlComponent title="" content={description} />

      {/* supporters logos bloc */}
      <SupportersBloc display={['flex', undefined, undefined, undefined, 'none']} pt={6}>
        <H2>{formatMessage({ id: 'program.supporters', defaultMessage: 'Supporters' })}</H2>
        <InfoHtmlComponent title="" content={enablers} />
        <a href="mailto:hello@jogl.io">
          {formatMessage({
            id: 'program.supporters.interested',
            defaultMessage: 'Interested in becoming a supporter?',
          })}
        </a>
      </SupportersBloc>

      {/* display program dates info, and status */}
      <Box pt={2}>
        <Box row>
          <Box pr={1}>
            <strong>{formatMessage({ id: `entity.info.launch_date`, defaultMessage: 'Launch' })}:</strong>
          </Box>
          {launchDate ? (
            <FormattedDate value={launchDate} year="numeric" month="long" day="2-digit" />
          ) : (
            formatMessage({ id: `general.noDate`, defaultMessage: 'No date' })
          )}
        </Box>
        <Box row>
          <Box pr={1}>
            <strong>{formatMessage({ id: 'attach.status', defaultMessage: 'Status: ' })}</strong>
          </Box>
          {formatMessage({ id: `entity.info.status.${status}`, defaultMessage: status })}
        </Box>
      </Box>

      {/* active challenges bloc */}
      {/* <Box py={8}>
        <H2>{formatMessage({ id: 'program.home.challengeActive', defaultMessage: 'Active challenges' })}</H2>
        {!dataChallenges ? (
          <Loading />
        ) : dataChallenges?.challenges.length === 0 ? (
          <NoResults type="challenge" />
        ) : (
          <>
            <P>
              {formatMessage({
                id: 'program.home.challengeCta',
                defaultMessage: 'Get involved in Challenges by submitting a project, evaluating submissions, & voting',
              })}
            </P>
            <Carousel spaceX={12}>
              {dataChallenges.challenges
                ?.filter(({ status }) => status !== 'draft')
                .map(({ title, short_title, logo_url, status }, index) => (
                  <ChallengeMiniCard
                    key={index}
                    icon={logo_url}
                    title={title}
                    shortTitle={short_title}
                    status={status}
                  />
                ))}
            </Carousel>
          </>
        )}
      </Box> */}

      {/* program boards bloc */}
      <H2>{formatMessage({ id: 'program.boardTitle', defaultMessage: 'Boards' })}</H2>
      {!dataBoards ? (
        <Loading />
      ) : dataBoards?.length === 0 ? (
        <NoResults type="board" />
      ) : (
        <>
          <P>
            {formatMessage({
              id: 'program.board.message',
              defaultMessage:
                'The following boards, composed of volunteers, are essential to the program. We invite anyone interested in contributing to join!',
            })}
          </P>
          <Carousel spaceX={12}>
            {dataBoards
              ?.sort(function (a, b) {
                return a.id - b.id; // sort by id (asc)
              })
              .map(({ title, description, users }, index) => {
                return (
                  <BoardCard key={index} width="16rem" height="18rem" justifyContent="space-between">
                    <Box>
                      <H2 onClick={() => showBoardModal(title, description, users)} as="button" fontSize="1.5rem">
                        {title}
                      </H2>
                      <Box maxHeight="9.5rem" overflow="hidden" position="relative">
                        <InfoHtmlComponent title="" content={description} />
                        {description.length > 150 && ( // show view more link only of text has more than 15O char
                          <ViewMore onClick={() => showBoardModal(title, description, users)} as="button">
                            ...{formatMessage({ id: 'general.showmore', defaultMessage: 'Show More' })}
                          </ViewMore>
                        )}
                      </Box>
                    </Box>
                    {users.length !== 0 && (
                      <Box row alignItems="center" pt={2}>
                        {/* show only 3 first members */}
                        {users?.slice(0, 3).map(({ logo_url_sm, id, first_name, last_name }, index) => (
                          <Link href="/user/[id]" as={`/user/${id}`} key={index}>
                            <a>
                              <Img width="30px" height="30px" src={logo_url_sm} alt={`${first_name} ${last_name}`} />
                            </a>
                          </Link>
                        ))}
                        {users.length > 3 && <MoreMembers>{`+${users.length - 3}`}</MoreMembers>}
                        <Box pl={4}>
                          {users.length}&nbsp;{formatMessage({ id: 'users.members', defaultMessage: 'members' })}
                        </Box>
                      </Box>
                    )}
                  </BoardCard>
                );
              })}
          </Carousel>
        </>
      )}
    </>
  );
};

const BoardMembersModal = ({ description, users, showModal, setIsOpen }) => (
  <Box width="100%">
    <InfoHtmlComponent title="" content={description} />
    {users.length !== 0 && (
      <Box row justifyContent="space-around" pt={5} flexWrap="wrap">
        {users?.map(({ logo_url, id, first_name, last_name, short_bio }, index) => (
          <Card key={index} justifyContent="center" width="17rem" containerStyle={{ margin: '0 1.1rem 1.3rem' }}>
            <Link href="/user/[id]" as={`/user/${id}`} key={index}>
              <a style={{ textAlign: 'center' }}>
                <Img width="50px" height="50px" src={logo_url} alt={`${first_name} ${last_name}`} />
                <P fontWeight="bold" fontSize="1.2rem" pt={2}>
                  {first_name + ' ' + last_name}
                </P>
              </a>
            </Link>
            <P mt={0}>{short_bio || '_ _'}</P>
            <Box alignItems="center">
              <Box
                as="button"
                onClick={() => {
                  // capture the opening of the modal as a special modal page view to google analytics
                  ReactGA.modalview('/send-message');
                  showModal({
                    children: <ContactForm itemId={id} closeModal={() => setIsOpen(false)} />,
                    title: 'Send message to {userFullName}',
                    titleId: 'user.contactModal.title',
                    values: { userFullName: first_name + ' ' + last_name },
                  });
                }}
              >
                <ContactButton icon="envelope" size="lg" />
              </Box>
            </Box>
          </Card>
        ))}
      </Box>
    )}
  </Box>
);

const ContactButton = styled(FontAwesomeIcon)`
  color: ${(p) => p.theme.colors.greys['700']};
  :hover {
    cursor: pointer;
    color: ${(p) => p.theme.colors.primary};
  }
`;

const SupportersBloc = styled(Box)`
  .infoHtml img {
    max-width: 300px !important;
  }
`;

const Img = styled.img`
  border-radius: 50%;
  object-fit: cover;
  margin-right: -4px;
`;

const MoreMembers = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  object-fit: cover;
  background: ${(p) => p.theme.colors.greys['200']};
  text-align: center;
  line-height: 30px;
  font-size: 14px;
`;

export default ProgramAbout;
