import { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import TitleInfo from '~/components/Tools/TitleInfo';
import ReactTooltip from 'react-tooltip';
// import "./FormInterestsComponent.scss";

class FormInterestsComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      content: [],
      errorCodeMessage: '',
      id: 'default',
      isValid: undefined,
      mandatory: false,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      title: 'Title',
    };
  }

  handleChange(event) {
    const actualContent = this.props.content;
    const interestSelected = Number(event.target.id.split('-')[1]);
    const elementClicked = actualContent.indexOf(interestSelected);
    if (elementClicked >= 0) {
      actualContent.splice(elementClicked, 1);
    } else {
      actualContent.push(interestSelected);
      actualContent.sort((a, b) => a - b);
    }
    this.props.onChange('interests', actualContent);
  }

  renderImgInterest(content, interest) {
    const path =
      this.props.intl.locale === 'fr'
        ? 'interests/fr' // if website or browser is in french
        : 'interests'; // if english
    if (
      content.find((element) => {
        return element === interest;
      })
    ) {
      return (
        <>
          <img src={`/images/${path}/Interest-${interest}.png`} className="imgInterest" alt={`Interest-${interest}`} />
        </>
      );
    }
    return (
      <img
        src={`/images/${path}/Interest-invert-${interest}.png`}
        className="imgInterest"
        alt={`Interest-${interest}`}
      />
    );
  }

  render() {
    const { errorCodeMessage, id, isValid, mandatory, title, intl } = this.props;

    const content = this.props.content.map((interest) => {
      if (interest.id) {
        return interest.id;
      }
      return interest;
    });

    const defaultInterests = [];
    for (let i = 1; i < 18; i++) {
      defaultInterests.push(i);
    }

    const tooltipMessage = intl.formatMessage({
      id: 'general.sdgs.tooltip',
      defaultMessage:
        "The United Nations has defined 17 Sustainable Development Goals (SDGs), to solve humanity's most urgent challenges. Please select at least one of them",
    });

    return (
      <div className="formInterests">
        <TitleInfo title={title} mandatory={mandatory} tooltipMessage={tooltipMessage} />
        <div className="content" id="interests">
          <div
            className="interestsContainer"
            style={isValid !== undefined ? (isValid ? {} : { border: '1px solid red' }) : {}}
          >
            {defaultInterests.map((interestId, index) => {
              return (
                <div className="interest" key={index}>
                  <input
                    id={`sdg-${interestId}`}
                    className="interestElement"
                    type="checkbox"
                    checked={content.find((element) => element === interestId) === true}
                    onChange={this.handleChange}
                  />
                  <label
                    htmlFor={`sdg-${interestId}`}
                    data-tip={intl.formatMessage({
                      id: `sdg-description-${interestId}`,
                      defaultMessage: `SDG ${interestId} text explanation`,
                    })}
                    data-for={`sdg-${interestId}`}
                  >
                    {this.renderImgInterest(content, interestId)}
                  </label>
                  <ReactTooltip
                    id={`sdg-${interestId}`}
                    effect="solid"
                    type="info"
                    className="solid-tooltip"
                    arrowColor="transparent"
                    overridePosition={({ left, top }, _e, _t, node) => {
                      return {
                        top,
                        left: typeof node === 'string' ? left : Math.max(left, 20),
                      };
                    }}
                  />
                </div>
              );
            })}
          </div>
          {errorCodeMessage && (
            <div className="invalid-feedback" style={{ display: 'inline' }}>
              <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default injectIntl(FormInterestsComponent);
