import TitleInfo from '~/components/Tools/TitleInfo';
import { useIntl } from 'react-intl';
import Select from 'react-select';
import { FC } from 'react';

interface Props {
  id: number;
  title: any;
  content: string;
  options: any[];
  onChange?: (id: any, value: any) => void;
  mandatory?: boolean;
  type?: string;
  warningMsgIntl?: any;
  errorCodeMessage: any;
}

const FormDropdownComponent: FC<Props> = ({
  id,
  title,
  content = '',
  options = [],
  onChange = (id, value) => console.warn(`onChange doesn't exist to update ${value} of ${id}`),
  mandatory = false,
  type = '',
  warningMsgIntl,
  errorCodeMessage,
}) => {
  const { formatMessage } = useIntl();
  const handleChange = (key, content) => {
    onChange(content.name, key.value);
  };

  const optionTranslation = (option) =>
    id === 'name' // if id of form is name, just display status
      ? option
      : id === 'category' // if dropdown is a user category, show them
      ? formatMessage({ id: `user.profile.edit.select.${option}`, defaultMessage: option })
      : type === 'challenge' // if dropdown comes from challenge, display special challenge status translation
      ? formatMessage({ id: `challenge.info.status.${option}`, defaultMessage: option })
      : // else display default status translation
        formatMessage({ id: `entity.info.status.${option}`, defaultMessage: option });

  const optionsList = options.map((option) => {
    return {
      value: option,
      label: optionTranslation(option),
    };
  });
  const customStyles = {
    option: (provided) => ({
      ...provided,
      padding: '6px 10px',
    }),
  };

  return (
    <div className="formDropdown">
      <TitleInfo title={title} mandatory={mandatory} />
      <div className="content">
        {warningMsgIntl &&
        content === 'draft' && ( // display warning message if applicable
            <p className="dropdownWarningMsg">
              {formatMessage({ id: warningMsgIntl.id, defaultMessage: warningMsgIntl.defaultMessage })}
            </p>
          )}
        <Select
          name={id}
          id={id}
          options={optionsList}
          styles={customStyles}
          isSearchable={false}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={formatMessage({
            id: 'user.profile.edit.select.default',
            defaultMessage: 'Select a category',
          })}
          defaultValue={content && { value: content, label: optionTranslation(content) }}
          noOptionsMessage={() => null}
          onChange={handleChange}
        />
      </div>
      {errorCodeMessage && (
        <div className="invalid-feedback" style={{ display: 'block' }}>
          {formatMessage({ id: errorCodeMessage, defaultMessage: 'Value is not valid' })}
        </div>
      )}
    </div>
  );
};
export default FormDropdownComponent;
