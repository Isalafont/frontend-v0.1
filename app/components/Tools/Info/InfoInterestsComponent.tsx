import { FormattedMessage, useIntl } from 'react-intl';
// import "./InfoInterestsComponent.scss";
import $ from 'jquery';
import TitleInfo from '~/components/Tools/TitleInfo';
import Box from '~/components/Box';
import ReactTooltip from 'react-tooltip';

export default function InfoInterestsComponent({ title = 'Title', content = [] }) {
  const intl = useIntl();
  const showMore = (items) => {
    $('.less').show();
    $(`.interest:lt(${items})`).show(300);
    $('.more').hide();
  };

  const showLess = () => {
    $('.interest').not(':lt(4)').hide(300);
    $('.more').show();
    $('.less').hide();
  };
  let titleInfo = '';

  if (title) {
    titleInfo = <TitleInfo title={title} />;
  }
  const path =
    intl.locale === 'fr'
      ? 'interests/fr' // if website or browser is in french
      : 'interests'; // if english
  let interestClass = '';
  if (content.length > 0) {
    return (
      <div className="infoInterests">
        {titleInfo}
        <div className="interests">
          {content.map((interestId, index) => {
            interestClass = index < 4 ? 'interest' : 'interest toggle';
            return (
              <div className={interestClass} key={index}>
                <img
                  src={`/images/${path}/Interest-${interestId}.png`}
                  className="imgInterest"
                  alt={`SDG ${interestId}`}
                  data-tip={intl.formatMessage({
                    id: `sdg-description-${interestId}`,
                    defaultMessage: `SDG ${interestId} text explanation`,
                  })}
                  data-for={`sdg-${interestId}`}
                />
                <ReactTooltip
                  id={`sdg-${interestId}`}
                  effect="solid"
                  type="info"
                  className="solid-tooltip"
                  arrowColor="transparent"
                  overridePosition={({ left, top }, _e, _t, node) => {
                    return {
                      top,
                      left: typeof node === 'string' ? left : Math.max(left, 20),
                    };
                  }}
                />
              </div>
            );
          })}
          {content.length > 4 && (
            <>
              <Box as="button" className="more" onClick={() => showMore(content.length)}>
                <FormattedMessage id="general.showmore" defaultMessage="Show More" />
              </Box>
              <Box as="button" className="less" onClick={() => showLess()}>
                <FormattedMessage id="general.showless" defaultMessage="Show Less" />
              </Box>
            </>
          )}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
