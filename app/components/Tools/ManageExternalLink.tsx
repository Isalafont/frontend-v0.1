import React, { FC, FormEvent, useState } from 'react';
import { useIntl } from 'react-intl';
import Box from '~/components/Box';
import Button from '~/components/primitives/Button';
import H2 from '~/components/primitives/H2';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import Card from '../Card';
import Grid from '../Grid';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormDropdownComponent from './Forms/FormDropdownComponent';
import FormIconComponent from './Forms/FormIconComponent';

interface Link {
  icon_url: string;
  id: number;
  icon: any;
  name: string;
  url: string;
}
const ManageExternalLink = ({ itemType, itemId, showTitle = true }) => {
  const { formatMessage } = useIntl();
  const { data: linkList, mutate: linkListMutate, revalidate: linkListRevalidate } = useGet<Link[]>(
    `/api/${itemType}/${itemId}/links`
  );
  const [showLinkCreate, setShowLinkCreate] = useState(false);
  const addLink = () => {
    setShowLinkCreate(!showLinkCreate);
  };
  return (
    <Box spaceY={3}>
      {showTitle && <H2>{formatMessage({ id: 'general.externalLink.title', defaultMessage: 'External links' })}</H2>}
      <Button type="button" onClick={addLink}>
        {formatMessage({ id: 'general.add', defaultMessage: 'Add' })}
      </Button>
      {showLinkCreate && (
        <CardFormCreate
          itemType={itemType}
          itemId={itemId}
          onCreate={(newLink: Link) => {
            linkListMutate(
              {
                data: [...linkList, newLink],
              },
              false
            );
            setShowLinkCreate(false);
          }}
        />
      )}
      <Grid display="grid" gridTemplateColumns={['1fr', undefined, undefined, undefined, '1fr 1fr']} gridGap={5} pt={8}>
        {linkList &&
          [...linkList]
            .sort(function (a, b) {
              return a.id - b.id; // sort by id (asc)
            })
            .map((value, i) => (
              <Box key={i} pt={3}>
                <CardFormEdit
                  value={value}
                  itemType={itemType}
                  itemId={itemId}
                  onChange={(changedLink: Link) => {
                    linkListMutate(
                      {
                        data: linkList.map((link) => {
                          if (link.id === changedLink.id) return changedLink;
                          return link;
                        }),
                      },
                      false
                    );
                  }}
                  onDelete={(linkId: number) => {
                    linkListMutate(
                      {
                        // removes the link which has the same id as linkId
                        data: [...linkList].filter((link) => link.id !== linkId),
                      },
                      false
                    );
                  }}
                />
              </Box>
            ))}
      </Grid>
    </Box>
  );
};
interface ICardFormCreate {
  itemType: string;
  itemId: number;
  onCreate?: (newLink: Link) => void;
}
interface ICardFormEdit {
  value: Link;
  itemType: string;
  itemId: number;
  onDelete?: (linkId: number) => void;
  onChange?: (changedLink: Link) => void;
}
const CardFormCreate: FC<ICardFormCreate> = ({ itemType, itemId, onCreate }) => {
  const api = useApi();
  const { formatMessage } = useIntl();
  const [newIcon, setNewIcon] = useState();
  const [newLink, setNewLink] = useState<Link>({
    id: undefined,
    url: undefined,
    icon: '/images/icons/links-facebook.png',
    name: 'facebook',
    icon_url: undefined,
  });

  // handle every changes to the link
  const handleChange: (key: string, content: string) => void = (key, content) => {
    if (key === 'name') {
      const theIcon = `/images/icons/links-${content}.png`;
      setNewLink((prevLink) => ({ ...prevLink, ['icon']: theIcon }));
    }
    setNewLink((prevLink) => ({ ...prevLink, [key]: content }));
  };
  // handle upload of custom icon
  const handleIconUpload: (icon) => void = (icon) => {
    setNewIcon(icon);
  };
  // handle when submitting (update, or create), an link
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (newLink.url !== '' && newLink.name !== '') {
      const bodyFormData = new FormData();
      bodyFormData.append('url', newLink.url);
      bodyFormData.append('name', newLink.name);
      newLink.name === 'custom' && bodyFormData.append('icon', newIcon);
      const config = {
        headers: { 'Content-Type': 'multipart/form-data' },
      };
      api
        .post(`/api/${itemType}/${itemId}/links`, bodyFormData, config)
        .then((res) => {
          onCreate(res.data); // call onCreate function
          setNewLink({ url: '', icon: '/images/icons/links-facebook.png', name: 'facebook' }); // reset link fields
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      alert('Please fill the url field');
    }
  };
  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <Box flexDirection={['column', undefined, 'row']} justifyContent="space-between">
          <Box width={'100%'} pr={[0, undefined, 5]}>
            <Box row flexDirection={['column', 'row']}>
              <FormDropdownComponent
                id="name"
                title={formatMessage({ id: 'general.externalLink.website', defaultMessage: 'External website' })}
                content={newLink.name}
                // prettier-ignore
                options={['facebook', 'linkedin', 'twitter', 'slack', 'instagram', 'github', 'gitlab',
                'dribbble', 'drive', 'dropbox', 'medium', 'skype', 'vimeo', 'youtube', 'custom']}
                onChange={handleChange}
              />
              <FormIconComponent
                id="icon"
                itemId={itemId}
                fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                maxSizeFile={4194304}
                itemType="users"
                imageUrl={newLink?.icon}
                isCustomLink={newLink.name === 'custom' ? true : false}
                onIconUpload={handleIconUpload}
              />
            </Box>
            <FormDefaultComponent
              id="url"
              content={newLink.url}
              title={formatMessage({ id: 'general.externalLink.url', defaultMessage: 'Url' })}
              placeholder={formatMessage({
                id: 'general.externalLink.url.placeholder',
                defaultMessage: 'https://www.facebook.com/justonegiantlab/',
              })}
              onChange={handleChange}
            />
          </Box>
          <Box spaceY={2} justifyContent="center" alignItems="flex-end" pt={[4, undefined, 0]}>
            <Button type="submit" width="100%">
              {formatMessage({ id: 'general.add', defaultMessage: 'Add' })}
            </Button>
          </Box>
        </Box>
      </form>
    </Card>
  );
};

const CardFormEdit: FC<ICardFormEdit> = ({ value, itemType, itemId, onDelete, onChange }) => {
  const api = useApi();
  const { formatMessage } = useIntl();
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  // const [newIcon, setNewIcon] = useState();
  // handle every changes to the link
  const handleChange: (key: string, content: string) => void = (key, content) => {
    if (key === 'name') {
      const theIcon = `/images/icons/links-${content}.png`;
      // setLink((prevLink) => ({ ...prevLink, ['icon']: theIcon }));
      onChange({ ...value, icon: theIcon });
    } else onChange({ ...value, [key]: content });
    setShowSubmitBtn(content !== value.url || content !== value.icon || content !== value.name);
  };
  // handle upload of custom icon
  const handleIconUpload: (icon) => void = (icon) => {
    // setNewIcon(icon);
  };
  // handle when submitting (update, or create), an link
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (value.url !== '' && value.name !== '') {
      api.patch(`/api/${itemType}/${itemId}/links/${value.id}`, value).then((res) => {
        onChange(value); // call onChange function
        setShowSubmitBtn(false); // hide update button
      });
    } else {
      alert('Please fill the url field');
    }
  };
  // handle when deleting on link
  const handleDelete = () => {
    api.delete(`/api/${itemType}/${itemId}/links/${value.id}`).then((res) => {
      onDelete(value.id); // call onDelete function
    });
  };

  return (
    <Card overflow="show">
      <form onSubmit={handleSubmit}>
        <Box flexDirection={['column', undefined, 'row']} justifyContent="space-between">
          <Box width={'100%'} pr={[0, undefined, 5]}>
            <Box row flexDirection={['column', 'row']}>
              <FormDropdownComponent
                id="name"
                title={formatMessage({ id: 'general.externalLink.website', defaultMessage: 'External website' })}
                content={value.name}
                // prettier-ignore
                options={['facebook', 'linkedin', 'twitter', 'slack', 'instagram', 'github', 'gitlab',
                'dribbble', 'drive', 'dropbox', 'medium', 'skype', 'vimeo', 'youtube', 'custom']}
                onChange={handleChange}
              />
              <FormIconComponent
                id="icon"
                itemId={itemId}
                fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                maxSizeFile={4194304}
                itemType="users"
                imageUrl={value?.icon_url}
                isCustomLink={value.name === 'custom' ? true : false}
                onIconUpload={handleIconUpload}
              />
            </Box>
            <FormDefaultComponent
              id="url"
              content={value.url}
              title={formatMessage({ id: 'general.externalLink.url', defaultMessage: 'Url' })}
              placeholder={formatMessage({
                id: 'general.externalLink.url.placeholder',
                defaultMessage: 'https://www.facebook.com/justonegiantlab/',
              })}
              onChange={handleChange}
            />
          </Box>
          <Box spaceY={2} justifyContent="center" alignItems="flex-end" pt={[4, undefined, 0]}>
            {showSubmitBtn && (
              <Button type="submit" width="100%">
                {formatMessage({ id: 'entity.form.btnUpdate', defaultMessage: 'Update' })}
              </Button>
            )}
            <Button type="button" btnType="danger" onClick={handleDelete} width="100%">
              {formatMessage({ id: 'feed.object.delete', defaultMessage: 'Delete' })}
            </Button>
          </Box>
        </Box>
      </form>
    </Card>
  );
};

export default ManageExternalLink;
