import { FC } from 'react';
import Box from '../Box';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ReactTooltip from 'react-tooltip';

interface Props {
  title: string;
  mandatory?: boolean;
  tooltipMessage?: string;
}

const TitleInfo: FC<Props> = ({ title, mandatory, tooltipMessage }) => (
  <Box row alignItems="center" className="titleInfo">
    {title}
    {mandatory && '*'}
    {tooltipMessage && (
      // show tooltip if have one
      <Box pl={2}>
        <FontAwesomeIcon
          icon="question-circle"
          data-tip={tooltipMessage}
          data-for="bannerTooltip"
          tabIndex={0}
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
        />
        <ReactTooltip
          id="bannerTooltip"
          effect="solid"
          type="dark"
          className="solid-tooltip"
          overridePosition={({ left, top }, _e, _t, node) => {
            return {
              top,
              left: typeof node === 'string' ? left : Math.max(left, 20),
            };
          }}
        />
      </Box>
    )}
  </Box>
);

export default TitleInfo;
