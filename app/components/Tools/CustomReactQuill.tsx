import ReactQuill, { Quill } from 'react-quill';
import { ImageDrop } from 'quill-image-drop-module';
import ImageResize from 'quill-image-resize-module-react';
import quillEmoji from 'quill-emoji';
import BlotFormatter from 'quill-blot-formatter';
import 'quill-mention';

Quill.register('modules/imageDrop', ImageDrop);
Quill.register('modules/imageResize', ImageResize);
Quill.register(
  {
    // emojis support
    'formats/emoji': quillEmoji.EmojiBlot,
    'modules/emoji-shortname': quillEmoji.ShortNameEmoji,
    'modules/emoji-toolbar': quillEmoji.ToolbarEmoji,
    'modules/emoji-textarea': quillEmoji.TextAreaEmoji,
  },
  true
);
Quill.register('modules/blotFormatter', BlotFormatter);

export default function CustomReactQuill(props) {
  return <ReactQuill {...props} />;
}
