import { FC } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import mimetype2fa from './mimetypes';
import { useApi } from '~/contexts/apiContext';
import ReactTooltip from 'react-tooltip';

interface Props {
  document: { content_type: string; filename: string; url: string; id: number };
  item: any;
  itemType: any;
  refresh: () => void;
}
const DocumentCard: FC<Props> = ({
  document = {
    content_type: 'image/jpg',
    filename: 'MyImage.jpg',
    url: 'https://something.com/image.jpg',
  },
  item,
  itemType,
  refresh,
}) => {
  const api = useApi();
  const intl = useIntl();

  const deleteDoc = () => {
    api
      .delete(`/api/${itemType}/${item.id}/documents/${document.id}`)
      .then((res) => {
        if (res.status === 200) {
          refresh();
        }
      })
      .catch((err) => {
        console.error(`Couldn't DELETE ${itemType} with itemId=${item.id}`, err);
      });
  };

  return (
    <div className="card documentCard">
      <div className="card-body">
        <h6 className="card-title">{document.filename}</h6>
        <p>
          <FontAwesomeIcon icon={mimetype2fa(document.content_type)} />
        </p>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={document.url}
          className="btn btn-secondary"
          download={document.url}
        >
          <FormattedMessage id="document.download" defaultMessage="Download" />
        </a>
        {/* restrict deletion of documents only to admin */}
        {item.is_admin && (
          <>
            <FontAwesomeIcon
              icon="times"
              onClick={deleteDoc}
              onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && deleteDoc()}
              data-tip={intl.formatMessage({ id: 'member.remove', defaultMessage: 'delete' })}
              data-for="document_delete"
              // show/hide tooltip on element focus/blur
              onFocus={(e) => ReactTooltip.show(e.target)}
              onBlur={(e) => ReactTooltip.hide(e.target)}
              tabIndex={0}
            />
            <ReactTooltip id="document_delete" effect="solid" />
          </>
        )}
      </div>
    </div>
  );
};
export default DocumentCard;
