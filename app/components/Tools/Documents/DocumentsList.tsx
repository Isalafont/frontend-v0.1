import { FC } from 'react';
import DocumentCard from './DocumentCard';
import DocumentCardFeed from './DocumentCardFeed';

interface Props {
  documents: any[];
  cardType: 'feed' | 'cards';
  item: any;
  postId?: number;
  itemType: any;
  isEditing?: boolean;
  refresh?: () => void;
}
const DocumentsList: FC<Props> = ({ documents = [], cardType, isEditing = false, item, itemType, postId, refresh }) => {
  const makeCards = () => {
    const cards = documents.map((document, index) => {
      if (document) {
        if (cardType === 'feed') {
          return (
            <DocumentCardFeed document={document} key={index} isEditing={isEditing} postId={postId} refresh={refresh} />
          );
        }
        return <DocumentCard document={document} key={index} item={item} itemType={itemType} refresh={refresh} />;
      }
      return '';
    });
    return cards;
  };

  return (
    <div className="feedDocumentList container-fluid">
      <div className="row">{makeCards()}</div>
    </div>
  );
};

export default DocumentsList;
