import React, { FC, useEffect, useState } from 'react';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import { ItemType } from '~/types';
import Grid from '../Grid';
import PeopleCard from '../People/PeopleCard';

interface Props {
  itemId: number;
  itemType: ItemType;
}
const ListFollowers: FC<Props> = ({ itemId, itemType }) => {
  const [listFollowers, setListFollowers] = useState([]);
  const [loading, setLoading] = useState(true);
  const api = useApi();
  useEffect(() => {
    setLoading(true);
    api
      .get(`/api/${itemType}/${itemId}/followers`)
      .then((res) => {
        const listUser = res.data.followers;
        setLoading(false);
        setListFollowers(listUser);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [api, itemId, itemType]); // = componentDidMount

  return (
    <div className="listFollowers">
      <Loading active={loading}>
        <Grid gridGap={[2, 4]} gridCols={[1, null, null, 2]} display={['grid', 'inline-grid']}>
          {listFollowers?.map((people, i) => (
            <PeopleCard
              key={i}
              id={people.id}
              firstName={people.first_name}
              lastName={people.last_name}
              nickName={people.nickname}
              shortBio={people.short_bio}
              logoUrl={people.logo_url}
              canContact={people.can_contact}
              hasFollowed={people.has_followed}
              projectsCount={people.projects_count}
              mutualCount={people.mutual_count}
            />
          ))}
        </Grid>
      </Loading>
    </div>
  );
};
export default ListFollowers;
