import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React, { FC, memo } from 'react';
import { useIntl } from 'react-intl';
import H2 from '~/components/primitives/H2';
import { useModal } from '~/contexts/modalContext';
// import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { DataSource } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import Box from '../Box';
import Card from '../Card';
import Chips from '../Chip/Chips';
import Title from '../primitives/Title';
import BtnFollow from '../Tools/BtnFollow';
import { ContactForm } from '../Tools/ContactForm';
import ReactTooltip from 'react-tooltip';
import ReactGA from 'react-ga';

interface BackgroundImgProps {
  url: string;
  logoSize?: string;
}
const BackgroundImg = styled.div<BackgroundImgProps>`
  background-image: url(${(p) => p.url});
  background-position: center center;
  width: ${(p) => p.logoSize};
  height: ${(p) => p.logoSize};
  margin-right: 10px;
  background-size: cover;
  border-radius: 50%;
  &:hover {
    opacity: 0.9;
  }
  @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
    width: 5rem;
    height: 5rem;
  }
  @media (max-width: 480px) {
    width: 4rem;
    height: 4rem;
  }
`;

const RoleChip = styled('div')`
  border-radius: 20px;
  background: pink;
  padding: 3.5px 10px;
  width: fit-content;
  margin-right: 0.5rem;
`;

const ContactButton = styled(FontAwesomeIcon)`
  color: ${(p) => p.theme.colors.greys['700']};
  :hover {
    cursor: pointer;
    color: ${(p) => p.theme.colors.primary};
  }
`;

const ShortBio = styled(Box)`
  overflow: hidden;
  word-break: break-word;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

const CardContainer = styled(Box)`
  @media (max-width: 480px) {
    flex-direction: column;
  }
  @media (min-width: 480px) and (max-width: 640px) {
    flex-direction: row;
  }
  @media (min-width: 900px) {
    flex-direction: row;
  }
`;

interface Props {
  id: number;
  firstName: string;
  lastName: string;
  nickName?: string;
  shortBio: string;
  skills?: string[];
  resources?: string[];
  status?: string;
  lastActive?: string;
  logoUrl: string;
  hasFollowed?: boolean;
  canContact?: boolean;
  source?: DataSource;
  logoSize?: string;
  role?: string;
  projectsCount?: number;
  mutualCount?: number;
  noMobileBorder?: boolean;
}
const PeopleCard: FC<Props> = ({
  id,
  firstName = 'First name',
  lastName = 'Last name',
  nickName = '',
  shortBio = '_ _',
  skills = [],
  resources = [],
  status,
  logoUrl = '/images/default/default-user.png',
  hasFollowed,
  canContact,
  lastActive,
  logoSize = '4rem',
  source,
  role,
  mutualCount,
  projectsCount,
  noMobileBorder = true,
}) => {
  const { userData } = useUserData();
  const { formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const theme = useTheme();
  // call api of mutual count here, as we don't want to do the api call at all places where PeopleCard is called
  // const { data: mutualCounts } = useGet<{ id: number; first_name: string; last_name: string; logo_url_sm: string }[]>(
  // const { data: mutualCounts } = useGet<[]>(userData && `/api/users/${id}/mutual`);

  const openMessageModal = () => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={() => setIsOpen(false)} />,
      title: 'Send message to {userFullName}',
      titleId: 'user.contactModal.title',
      values: { userFullName: firstName + ' ' + lastName },
    });
  };

  const userUrl = { href: `/user/[id]/[[...index]]`, as: `/user/${id}/${nickName}` };
  return (
    // noMobileBorder param will remove border and padding of card unless noMobileBorder is false
    <Card spaceY={3} noMobileBorder={noMobileBorder}>
      <CardContainer justifyContent="space-between">
        <Box row>
          <Link href={userUrl.href} as={userUrl.as} passHref>
            <a>
              <BackgroundImg url={logoUrl} logoSize={logoSize} />
            </a>
          </Link>
          <Box justifyContent="center" px={2}>
            <Box row flexWrap="wrap" alignItems="center">
              <Link href={userUrl.href} as={userUrl.as} passHref>
                <Title pr={2}>
                  <H2 fontSize={'4xl'}>
                    {firstName} {lastName}
                  </H2>
                </Title>
              </Link>
              {role && <RoleChip>{formatMessage({ id: `member.role.${role}`, defaultMessage: role })}</RoleChip>}
              {canContact !== undefined && userData && userData.id !== id && canContact !== false && (
                <>
                  <ContactButton
                    icon="envelope"
                    onClick={openMessageModal}
                    onKeyUp={(e) =>
                      // execute only if it's the 'enter' key
                      (e.which === 13 || e.keyCode === 13) && openMessageModal()
                    }
                    tabIndex={0}
                    data-tip={formatMessage(
                      {
                        id: 'user.contactModal.title',
                        defaultMessage: 'Send message to {userFullName}',
                      },
                      { userFullName: firstName + ' ' + lastName }
                    )}
                    data-for="contact"
                    // show/hide tooltip on element focus/blur
                    onFocus={(e) => ReactTooltip.show(e.target)}
                    onBlur={(e) => ReactTooltip.hide(e.target)}
                  />
                  <ReactTooltip id="contact" effect="solid" place="bottom" />
                </>
              )}
            </Box>
            <ShortBio my={1}>{shortBio || '_ _'}</ShortBio>
            {/* <P mb={0} color={theme.colors.greys['500']}>
              {status}
            </P> */}
          </Box>
        </Box>
        {/* <Box justifyContent="center" spaceY={1} mt={[3, undefined, 3, 0]}> */}
        <Box justifyContent="center" spaceY={1} mt={[3, undefined, 3, 0]} display={['none', 'flex']}>
          {(!userData || userData?.id !== id) && (hasFollowed !== undefined || source === 'algolia') && (
            <BtnFollow
              followState={hasFollowed}
              itemType="users"
              itemId={id}
              source={source}
              width={'6.25rem'}
              displayButton
            />
          )}
        </Box>
      </CardContainer>
      {(skills.length !== 0 || resources.length !== 0) && (
        <Box spaceY={2}>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              as: `/search/members/?refinementList[skills][0]=${skill}`,
              href: `/search/[active-index]/?refinementList[skills][0]=${skill}`,
            }))}
            overflowLink={{ href: '/user/[id]', as: `/user/${id}` }}
            color={theme.colors.primary}
            showCount={3}
            smallChips
          />
          <Chips
            data={resources.map((resource) => ({
              title: resource,
              as: `/search/members/?refinementList[ressources][0]=${resource}`,
              href: `/search/[active-index]/?refinementList[ressources][0]=${resource}`,
            }))}
            overflowLink={{ href: '/user/[id]', as: `/user/${id}` }}
            color={theme.colors.pink}
            showCount={3}
            smallChips
          />
        </Box>
      )}
      {(mutualCount > 0 || projectsCount > 0) && (
        <Box color={theme.colors.greys['700']} row pt={1} flexWrap="wrap" justifyContent="space-between">
          {projectsCount && (
            <Box pb={1} pr={6}>{`${formatMessage({
              id: 'user.info.currentlyOn',
              defaultMessage: 'Currently on',
            })} ${projectsCount} ${textWithPlural('project', projectsCount)}`}</Box>
          )}
          {mutualCount > 0 && userData?.id !== id && (
            <Box>{`${mutualCount} ${textWithPlural('mutualConnection', mutualCount)}`}</Box>
          )}
        </Box>
      )}
    </Card>
  );
};

export default memo(PeopleCard);
