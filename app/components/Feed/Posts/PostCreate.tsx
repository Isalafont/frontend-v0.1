import { useState, FC } from 'react';
import FormPost from './FormPost';
import { findMentions, transformMentions } from '~/components/Feed/Mentions';
import { useApi } from '~/contexts/apiContext';
import ReactGA from 'react-ga';

interface Props {
  feedId: number;
  content?: string;
  user: {
    id: number;
  };
  refresh?: () => void;
}
const PostCreate: FC<Props> = ({ feedId, content: contentProp = '', user, refresh: refreshProp }) => {
  const [content, setContent] = useState(contentProp);
  const [documents, setDocuments] = useState([]);
  const [uploading, setUploading] = useState(false);
  const api = useApi();
  const handleChange = (newContent) => {
    setContent(newContent);
  };

  const handleChangeDoc = (newDocuments) => {
    refreshProp();
    setDocuments(newDocuments);
  };
  const refresh = () => {
    refreshProp();
    setContent('');
    setUploading(false);
    setDocuments([]);
  };
  const handleSubmit = () => {
    const mentions = findMentions(content);
    const contentNoMentions = transformMentions(content);
    const userId = user.id;
    type PostJson = {
      post: {
        user_id: number;
        content: any;
        feed_id: number;
        mentions?: typeof mentions;
        documents?: typeof documents;
      };
    };
    const postJson: PostJson = {
      post: {
        user_id: userId,
        content: contentNoMentions,
        feed_id: feedId,
      },
    };
    if (mentions) {
      postJson.post.mentions = mentions;
    }
    if (documents) {
      postJson.post.documents = documents;
    }
    if (feedId !== undefined) {
      setUploading(true);
      api.post('/api/posts', postJson).then((res) => {
        // record event to Google Analytics
        ReactGA.event({ category: 'Post', action: 'post', label: `post ${res.data.id}` });
        if (documents.length > 0) {
          const itemId = res.data.id;
          const itemType = 'posts';
          const type = 'documents';
          if (itemId) {
            const bodyFormData = new FormData();
            Array.from(documents).forEach((file) => {
              bodyFormData.append(`${type}[]`, file);
            });

            const config = {
              headers: { 'Content-Type': 'multipart/form-data' },
            };

            api
              .post(`/api/${itemType}/${itemId}/documents`, bodyFormData, config)
              .then((res2) => {
                if (res2.status === 200) {
                  refresh();
                } else {
                  setUploading(false);
                }
              })
              .catch((err) => {
                console.error(`Couldn't POST ${itemType} of itemId=${itemId}`, err);
                setUploading(false);
              });
          } else {
            refresh();
          }
        } else {
          refresh();
        }
      });
    } else {
      console.warn('feedId is undefined', feedId);
    }
  };

  return (
    <FormPost
      action="create"
      content={content}
      documents={documents}
      handleChange={handleChange}
      handleChangeDoc={handleChangeDoc}
      handleSubmit={handleSubmit}
      uploading={uploading}
      user={user}
    />
  );
};
export default PostCreate;
