import { useState } from 'react';
import FormComment from './FormComment';
import { findMentions, transformMentions } from '~/components/Feed/Mentions';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import { NotLoggedInModal } from '~/utils/getApi';
import ReactGA from 'react-ga';

export default function CommentCreate({ content: contentProp = '', postId = undefined, user, refresh }) {
  const [content, setContent] = useState(contentProp);
  const [uploading, setUploading] = useState(false);
  const modal = useModal();
  const api = useApi();
  const handleChange = (newContent) => {
    if (!user) {
      const { showModal, setIsOpen } = modal;
      showModal({
        children: <NotLoggedInModal hideModal={() => setIsOpen(false)} />,
        title: 'You must be logged in',
        titleId: 'footer.modalSignIn.title',
        maxWidth: '25rem',
      });
    }
    setContent(newContent);
  };

  const handleSubmit = () => {
    const mentions = findMentions(content);
    const contentNoMentions = transformMentions(content);
    const userId = user?.id;
    const commentJson = {
      comment: {
        user_id: userId,
        content: contentNoMentions,
      },
    };
    if (mentions) {
      commentJson.comment.mentions = mentions;
    }
    setUploading(true);
    api
      .post(`/api/posts/${postId}/comment`, commentJson)
      .then(() => {
        // record event to Google Analytics
        ReactGA.event({ category: 'Post', action: 'comment', label: `post ${postId}` });
        refresh();
        setUploading(false);
        setContent('');
        const commentBtn = document.querySelector(`.commentCreate#post${postId} .btn-primary`); // select comment button from parent post
        commentBtn.style.display = 'none'; // hide comment button after comment was posted
        // send event to google analytics
      })
      .catch((err) => {
        console.error(`Couldn't POST comment in post with id=${postId}`, err);
        setUploading(false);
      });
  };
  return (
    <FormComment
      action="create"
      postId={postId}
      content={content}
      handleChange={handleChange}
      handleSubmit={handleSubmit}
      uploading={uploading}
      user={user}
    />
  );
}
