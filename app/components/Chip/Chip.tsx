import React, { FC, forwardRef, useEffect } from 'react';
import ReactTooltip from 'react-tooltip';
import styled from '~/utils/styled';
import Box from '../Box';

interface Props {
  color?: string;
  maxLength?: number;
  children: string | string[];
  smallChips?: boolean;
}
// TODO: Explain why we use forwardRef here.
const Chip: FC<Props> = forwardRef(({ color = 'black', children, maxLength = 1000, smallChips }, ref) => {
  // add "..." to value if it's length is > than maxLength
  const value = children.length > maxLength ? `${children.substr(0, maxLength)}...` : children;
  useEffect(() => {
    ReactTooltip.rebuild();
  }, [value]);
  return (
    <>
      <Container
        ref={ref}
        border={`1px solid ${color}`}
        color={color}
        borderRadius={smallChips ? '1rem' : '5px'}
        height={smallChips ? '1.75rem' : '2.2rem'}
        fontWeight={smallChips ? '400' : '500'}
        justifyContent="center"
        alignItems="center"
        px={2}
        fontSize={smallChips ? 1 : '.9rem'}
        mr={smallChips ? 2 : '.65rem'}
        mb={smallChips ? 1 : '.6rem'}
        // add tooltip prop to show chip tooltip, it its length is more than maxLength
        {...(children.length > maxLength && { 'data-tip': children, 'data-for': `skillTooltip${children}` })}
      >
        {value}
      </Container>
      <ReactTooltip
        id={`skillTooltip${children}`}
        effect="solid"
        role="tooltip"
        type="dark"
        className="forceTooltipBg"
      />
    </>
  );
});

const Container = styled(Box)`
  white-space: nowrap;
  &:hover {
    background-color: ${(p) => p.color};
    color: white;
  }
`;

export default Chip;
