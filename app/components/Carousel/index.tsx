import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, useEffect, useRef, useState } from 'react';
import { SnapItem, SnapList, useDragToScroll, useScroll, useVisibleElements } from 'react-snaplist-carousel';
import { PaddingProps, space } from 'styled-system';
import styled from '~/utils/styled';
import Box from '../Box';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

interface Props {
  spaceX?: number;
  spaceY?: number;
  showDots?: boolean;
  paddingX?: PaddingProps;
  children: JSX.Element[];
}

const Carousel: FC<Props> = ({ spaceX = 0, spaceY = 0, children, showDots = true, paddingX = '0' }) => {
  const snapList = useRef(undefined);
  const [showControls, setShowControls] = useState(true);
  const [hideArrow, setHideArrow] = useState<undefined | 'left' | 'right'>('left');
  const selectItems = useVisibleElements({ debounce: 10, ref: snapList }, (elements) => elements);
  useEffect(() => {
    // Hide controls when there's no invisible elements aka the children don't overflow
    if (selectItems.length === children.length) {
      setShowControls(false);
    } else {
      setShowControls(true);
    }
    // Hide right arrow when last selectItems is visible
    if (children.length - 1 === selectItems[selectItems.length - 1]) {
      setHideArrow('right');
    } else if (selectItems[0] === 0) {
      // Hide left arrow when selectItems first visible element is the first children.
      setHideArrow('left');
    } else {
      setHideArrow(undefined);
    }
  }, [selectItems]);

  const goToElement = useScroll({ ref: snapList });
  useDragToScroll({ ref: snapList });

  return (
    <Container px={paddingX}>
      {showControls && (
        <>
          {hideArrow !== 'left' && (
            <ArrowLeft type="button" onClick={() => goToElement(selectItems[0] - 1)}>
              <FontAwesomeIcon icon={faArrowLeft} />
            </ArrowLeft>
          )}
          {hideArrow !== 'right' && (
            <ArrowRight type="button" onClick={() => goToElement(selectItems[selectItems.length - 1] + 1)}>
              <FontAwesomeIcon icon={faArrowRight} />
            </ArrowRight>
          )}
        </>
      )}

      <SnapList ref={snapList} width="100%" direction="horizontal">
        {children.map((child, i) => (
          <SnapItem
            key={i}
            margin={{
              // @ts-ignore
              left: i === 0 ? 0 : spaceX,
              // @ts-ignore
              right: i === children.length ? 0 : spaceX,
              // @ts-ignore
              top: spaceY,
              // @ts-ignore
              bottom: '.5rem',
            }}
            snapAlign={i === 0 ? 'start' : 'center'}
          >
            {child}
          </SnapItem>
        ))}
      </SnapList>
      {showDots && showControls && (
        <Box row justifyContent="center" pt={4} spaceX={2}>
          {children.map((_, index) => (
            <Dot key={index} active={selectItems.includes(index)} type="button" onClick={() => goToElement(index)} />
          ))}
        </Box>
      )}
    </Container>
  );
};
const Container = styled.div`
  ${space};
  position: relative;
`;
const ArrowLeft = styled.button`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  position: absolute;
  bottom: 0;
  left: 0;
  transform: translateY(-50%);
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  box-shadow: ${(props) => props.theme.shadows.default};
  background-color: white;
`;
const ArrowRight = styled.button`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  position: absolute;
  bottom: 0;
  right: 0;
  transform: translateY(-50%);
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  box-shadow: ${(props) => props.theme.shadows.default};
  background-color: white;
`;
interface DotProps {
  active: boolean;
}
const Dot = styled.button<DotProps>`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  border-radius: 50%;
  width: 10px;
  height: 10px;
  background-color: ${(p) => (p.active ? p.theme.colors.greys['800'] : p.theme.colors.greys['500'])};
  &:hover {
    background-color: ${(p) => p.theme.colors.greys['800']};
  }
`;

export default Carousel;
