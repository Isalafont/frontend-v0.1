import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React, { FC, useContext } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Feed from '~/components/Feed/Feed';
import BtnFollow from '~/components/Tools/BtnFollow';
import BtnJoin from '~/components/Tools/BtnJoin';
import ShareBtns from '~/components/Tools/ShareBtns/ShareBtns';
import { UserContext } from '~/contexts/UserProvider';
import useUserData from '~/hooks/useUserData';
import { NeedDisplayMode } from '~/pages/need/[id]';
import Box from '../Box';
import BtnSave from '../Tools/BtnSave';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import NeedDates from './NeedDates';
import NeedDelete from './NeedDelete';
import NeedDocsManagement from './NeedDocsManagement';
import NeedWorkers from './NeedWorkers';
import { Need } from '~/types';
import { useTheme } from 'emotion-theming';
import Chips from '../Chip/Chips';

interface Props {
  changeMode: (newMode: NeedDisplayMode) => void;
  need: Need;
  refresh: () => void;
}
const NeedContent: FC<Props> = ({
  changeMode = () => console.warn('Missing changeMode function'),
  need = {},
  refresh = () => console.warn('Missing refresh function'),
}) => {
  const user = useContext(UserContext);
  if (need === undefined) {
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }
  const { formatMessage } = useIntl();
  const { userData } = useUserData();
  const theme = useTheme();
  return (
    <div className={`needContent need${need.id}`}>
      <div className="needContent--header d-flex justify-content-between">
        <div className="d-block">
          <h4>{need.title}</h4>
        </div>
        <div className="need-manage right d-flex flex-row">
          {userData && (
            <>
              <Box pl={4} pr={2} alignItems="center">
                <BtnSave itemType="needs" itemId={need.id} saveState={need.has_saved} />
              </Box>
              <BtnFollow followState={need.has_followed} itemType="needs" itemId={need.id} />
            </>
          )}
          {need.is_owner && (
            <div className="btn-group dropright">
              <button
                type="button"
                className="btn btn-secondary dropdown-toggle"
                data-display="static"
                data-flip="false"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                •••
              </button>
              <div className="dropdown-menu dropdown-menu-right">
                <>
                  <Box
                    row
                    pb={1}
                    alignItems="center"
                    onClick={() => changeMode('update')}
                    onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && changeMode('update')}
                    tabIndex={0}
                  >
                    <FontAwesomeIcon icon="edit" />
                    <FormattedMessage id="feed.object.update" defaultMessage="Update" />
                  </Box>
                  <NeedDelete need={need} refresh={refresh} />
                </>
              </div>
            </div>
          )}
        </div>
      </div>
      <Box flexWrap="wrap" spaceY={2} spaceX={3} row alignItems="center">
        <ShareBtns type="need" needId={need.id} />
        {!need.is_owner && (
          <BtnJoin
            itemId={need.id}
            itemType="needs"
            joinState={need.is_member}
            textJoin={<FormattedMessage id="need.help.willHelp" defaultMessage="I'll help" />}
            textUnjoin={<FormattedMessage id="need.help.stopHelp" defaultMessage="I can't help" />}
          />
        )}
        {need.status === 'completed' && (
          <Box ml={3} className={`statusTag ${need.status} justify-content`}>
            <FormattedMessage id="need.completed" defaultMessage="Completed" />
          </Box>
        )}
      </Box>
      <div className="needProject">
        <FormattedMessage id="needsPage.project" defaultMessage="From project: " />
        <Link href="/project/[id]" as={`/project/${need.project.id}#needs`}>
          <a>
            {need.project.title} <FontAwesomeIcon icon="external-link-alt" />
          </a>
        </Link>
      </div>
      {/* show need publish AND/OR due date if one of them is set */}
      {(need.created_at || need.end_date) && (
        <NeedDates publishedDate={need.created_at} dueDate={need.end_date} status={need.status} />
      )}
      <div className="needContent--main">
        <InfoHtmlComponent title="" content={need.content} />
        {need.skills.length !== 0 && (
          <Box>
            <Box>{formatMessage({ id: 'need.skills.title', defaultMessage: 'Expected skills' })}</Box>
            <Chips
              data={need.skills.map((skill) => ({
                title: skill,
                as: `/search/needs/?refinementList[skills][0]=${skill}`,
                href: `/search/[active-index]/?refinementList[skills][0]=${skill}`,
              }))}
              color={theme.colors.primary}
              showCount={5}
              overflowLink="seeMore"
            />
          </Box>
        )}
        {need.ressources.length !== 0 && (
          <Box mt={3}>
            <Box>{formatMessage({ id: 'need.resources.title', defaultMessage: 'Expected resources' })}</Box>
            <Chips
              data={need.ressources.map((resource) => ({
                title: resource,
                as: `/search/needs/?refinementList[ressources][0]=${resource}`,
                href: `/search/[active-index]/?refinementList[ressources][0]=${resource}`,
              }))}
              color={theme.colors.pink}
              showCount={5}
              overflowLink="seeMore"
            />
          </Box>
        )}
        <NeedWorkers need={need} mode="details" />
        {need.documents.length > 0 && <NeedDocsManagement need={need} mode="details" />}
        <div className="needFeed">
          {(user.isConnected || (!user.isConnected && need.posts_count > 0)) && (
            <h6>
              <FormattedMessage id="need.feed.title" defaultMessage="Discussion and results" />
            </h6>
          )}
          <Feed displayCreate={true} feedId={need.feed_id} isAdmin={need.is_owner} />
        </div>
      </div>
    </div>
  );
};

export default NeedContent;
