import { FormattedMessage } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useApi } from '~/contexts/apiContext';

export default function NeedDelete({ need, refresh = () => console.warn('Missing refresh function') }) {
  const api = useApi();
  const deleteNeed = () => {
    if (need) {
      api
        .delete(`api/needs/${need.id}`)
        .then(() => {
          refresh();
        })
        .catch((error) => {
          console.error(error);
        });
    }
  };
  return (
    <div onClick={deleteNeed} onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && deleteNeed()} tabIndex={0}>
      <FontAwesomeIcon icon="trash" className="postDelete" />{' '}
      <FormattedMessage id="feed.object.delete" defaultMessage="Delete" />
    </div>
  );
}
