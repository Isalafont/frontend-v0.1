import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useIntl } from 'react-intl';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import { displayObjectDate, isDateUrgent } from '~/utils/utils';
import Box from '../Box';
import P from '../primitives/P';

interface Props {
  publishedDate: string;
  dueDate: string;
  status: string;
}
const NeedDates = ({ publishedDate, dueDate, status }: Props) => {
  const theme = useTheme();
  const { formatMessage } = useIntl();
  const getDaysLeft = (dueDate) => {
    const now = new Date();
    const end = new Date(dueDate);
    var daysLeft = Math.ceil((end - now) / 1000 / 60 / 60 / 24);
    if (daysLeft < 0) {
      daysLeft = -1;
    }
    return daysLeft;
  };

  return (
    <Box row alignItems="center" spaceX={2}>
      {/* show published date if we have it */}
      {publishedDate && (
        <P mb={0} color={theme.colors.greys['600']}>
          {`${formatMessage({ id: 'post.posted', defaultMessage: 'Posted' })}: `}
          {displayObjectDate(publishedDate)}
        </P>
      )}
      {/* separator if we have the 2 dates, and status is not completed, and if due date is still in future */}
      {dueDate && publishedDate && status !== 'completed' && getDaysLeft(dueDate) >= 0 && <Box>•</Box>}
      {/* if there is a due date and it's less than 10 days */}
      {dueDate && getDaysLeft(dueDate) <= 10 && status !== 'completed' && (
        <Box color={theme.colors.greys['600']}>
          {
            0 <= getDaysLeft(dueDate) === getDaysLeft(dueDate) < 1 &&
              formatMessage({ id: 'need.card.due.today', defaultMessage: 'Due today' }) // if date is today (between 0 and 1 day)
          }
          {getDaysLeft(dueDate) === 1 && // if date is tomorrow
            formatMessage({ id: 'need.card.due.tomorrow', defaultMessage: 'Due tomorrow' })}
          {getDaysLeft(dueDate) > 1 && // if date is more than tomorrow (and less then 10 days)
            formatMessage({ id: 'need.card.due.start', defaultMessage: 'Due in' }) +
              ` ${Math.ceil(getDaysLeft(dueDate))} ` +
              formatMessage({ id: 'need.card.due.end', defaultMessage: 'days' })}
        </Box>
      )}
      {/* if there is a due date and it's more than 10 days */}
      {dueDate &&
      getDaysLeft(dueDate) > 10 &&
      status !== 'completed' && ( // if due date is more than 10 days
          <P mb={0} color={theme.colors.greys['600']}>
            {`${formatMessage({ id: 'need.end_date', defaultMessage: 'Due on' })}: `}
            {displayObjectDate(dueDate)}
          </P>
        )}
      {/* if due date is less than 48 hours, show little 'bolt' icon */}
      {dueDate && isDateUrgent(dueDate) && status !== 'completed' && <BoltIcon icon="bolt" />}
    </Box>
  );
};

const BoltIcon = styled(FontAwesomeIcon)`
  background-color: ${(p) => p.theme.colors.orange};
  padding: 0.3rem;
  width: 1.8rem !important;
  height: 1.8rem !important;
  color: white;
  border-radius: 50%;
`;

export default NeedDates;
