import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { LayoutProps } from 'styled-system';
import Card from '~/components/Card';
import Chips from '~/components/Chip/Chips';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import ShareBtns from '~/components/Tools/ShareBtns/ShareBtns';
import { DataSource } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import Box from '../Box';
import A from '../primitives/A';
import P from '../primitives/P';
import NeedDates from './NeedDates';

interface Props {
  title: string;
  project?: any;
  hasSaved: boolean;
  postsCount?: number;
  skills?: string[];
  resources?: string[];
  publishedDate?: string;
  dueDate?: string;
  id: number;
  width?: LayoutProps['width'];
  cardFormat?: string;
  source?: DataSource;
  status?: string;
  membersCount?: number;
}
const NeedCard = ({
  title,
  project,
  skills = [],
  resources = [],
  publishedDate,
  dueDate,
  hasSaved,
  postsCount = 0,
  id,
  width,
  cardFormat,
  status,
  source,
  membersCount = 0,
}: Props) => {
  const theme = useTheme();
  const { formatMessage } = useIntl();
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';

  return (
    <Card spaceY={3} width={width}>
      <Box justifyContent="space-between" height="full">
        <Box spaceY={2}>
          <Box row justifyContent="space-between" spaceX={4}>
            <Link href="/need/[id]" as={`/need/${id}`} passHref>
              <Title pr={2}>
                <H2 fontSize={TitleFontSize}>{title}</H2>
              </Title>
            </Link>
            <BtnSave itemType="needs" itemId={id} saveState={hasSaved} source={source} />
          </Box>
          {project && (
            <P>
              {formatMessage({ id: 'needsPage.project', defaultMessage: 'From project: ' })}
              <A href="/project/[id]" as={`/project/${project.id}`}>
                {project.title}
              </A>
            </P>
          )}
          {cardFormat !== 'compact' && (
            <>
              <Chips
                data={skills.map((skill) => ({
                  title: skill,
                  as: `/search/needs/?refinementList[skills][0]=${skill}`,
                  href: `/search/[active-index]/?refinementList[skills][0]=${skill}`,
                }))}
                overflowLink={{ href: '/need/[id]', as: `/need/${id}` }}
                color={theme.colors.primary}
                showCount={2}
                smallChips
              />
              <Chips
                data={resources.map((resource) => ({
                  title: resource,
                  as: `/search/needs/?refinementList[ressources][0]=${resource}`,
                  href: `/search/[active-index]/?refinementList[ressources][0]=${resource}`,
                }))}
                overflowLink={{ href: '/need/[id]', as: `/need/${id}` }}
                color={theme.colors.pink}
                showCount={2}
                smallChips
              />
              {/* show need publish AND/OR due date if one of them is set */}
              {(publishedDate || dueDate) && (
                <NeedDates publishedDate={publishedDate} dueDate={dueDate} status={status} />
              )}
              {status === 'completed' && (
                <StatusChip>
                  <FormattedMessage id="entity.info.status.completed" defaultMessage="Completed" />
                </StatusChip>
              )}
            </>
          )}
        </Box>
        {cardFormat !== 'compact' && (
          <Box pt={5}>
            <Box row justifyContent="space-between" alignItems="center">
              {membersCount > 1 && (
                <Box row justifyContent="space-between">
                  {membersCount - 1} {textWithPlural('volunteer', membersCount)}
                </Box>
              )}
              {postsCount > 0 && (
                <Box row justifyContent="space-between">
                  {postsCount} {textWithPlural('post', postsCount)}
                </Box>
              )}
            </Box>
            <Box row justifyContent="space-between" borderTop={`1px solid ${theme.colors.grey}`} pt={2} mt={4}>
              <Link href="/need/[id]" as={`/need/${id}`}>
                <button className="btn-postcard btn" type="button">
                  <FontAwesomeIcon icon={['far', 'hand-paper']} size="lg" />
                  <FormattedMessage id="need.help.willHelp" defaultMessage="I'll help" />
                </button>
              </Link>
              <ShareBtns type="need" needId={id} />
            </Box>
          </Box>
        )}
      </Box>
    </Card>
  );
};

const StatusChip = styled('div')`
  border-radius: 20px;
  background: lightgray !important;
  padding: 3.5px 10px;
  width: fit-content;
  text-align: center;
`;

export default NeedCard;
