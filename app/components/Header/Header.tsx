import { faBars, faCheck, faGlobe, faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Menu, MenuLink } from '@reach/menu-button';
import cookie from 'js-cookie';
import Link from 'next/link';
import { Router, useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { Transition } from 'react-transition-group';
import A from '~/components/primitives/A';
import Button from '~/components/primitives/Button';
import UserMenu from '~/components/User/UserMenu';
import { useApi } from '~/contexts/apiContext';
import { UserContext } from '~/contexts/UserProvider';
import useGet from '~/hooks/useGet';
import { Program } from '~/types';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import Box from '../Box';
import Loading from '../Tools/Loading';
import {
  AMenu,
  Container,
  DesktopNav,
  DropDownMenu,
  LangItem,
  Logo,
  MobileNav,
  MobileSideNav,
  StyledMenuButton,
} from './Header.styles';
import { displayObjectRelativeDate } from '~/utils/utils';

const navLinks = [
  { title: 'Members', intlId: 'general.members', url: '/search/[active-index]', as: '/search/members' },
  { title: 'Projects', intlId: 'general.projects', url: '/search/[active-index]', as: '/search/projects' },
  { title: 'Needs', intlId: 'entity.tab.needs', url: '/search/[active-index]', as: '/search/needs' },
  { title: 'Groups', intlId: 'general.groups', url: '/search/[active-index]', as: '/search/groups' },
  { title: 'Challenges', intlId: 'general.challenges', url: '/search/[active-index]', as: '/search/challenges' },
];
const languages = [
  { id: 'en', title: 'English' },
  { id: 'fr', title: 'Français' },
  { id: 'de', title: 'Deutsch' },
  { id: 'es', title: 'Español' },
];
const Header = () => {
  const router = useRouter();
  const theme = useTheme();
  const user = useContext(UserContext);
  const [isOpen, setOpen] = useState(false);
  const { data: programs, error: programError } = useGet<Program[]>('/api/programs');

  const [notifications, setNotifications] = useState([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(0);
  // const [totalNotificationsCount, setTotalNotificationsCount] = useState([]);
  const api = useApi();

  useEffect(() => {
    user.user &&
      api
        .post('/api/graphql/', {
          query: `
          query {
            user (id: ${user.user.id} ) {
              id,
              email,
              notifications(first: 20) {
                unreadCount
                totalCount
                edges {
                  node {
                    id
                    link
                    read
                    subjectLine
                    createdAt
                  }
                }
              }
            }
          }
        `,
        })
        .then((response) => {
          var notifications = response.data.data.user.notifications;
          setUnreadNotificationsCount(notifications.unreadCount);
          // setTotalNotificationsCount(notifications.totalCount);
          setNotifications(notifications.edges.map((edge) => edge.node));
        })
        .catch((err) => {
          // do something
        });
  }, [user.user]);

  // force close mobile menu when changing page, as the href of some pages are the same(all search page)
  Router.events.on('routeChangeStart', () => setOpen(false));

  const gotoSignInPage = () => {
    router.push({
      pathname: '/signin',
      query: { callbackPath: router.pathname, callbackAsPath: router.asPath },
    });
  };

  const ProgramsDropDown = () => (
    <Menu>
      <StyledMenuButton>
        <FormattedMessage id="general.programs" defaultMessage="Programs" /> <span aria-hidden>▾</span>
      </StyledMenuButton>

      <DropDownMenu>
        {programs ? (
          programs?.reverse().map((program, i) => (
            // browse through programs (from newest to oldest)
            <Link key={i} href="/program/[short_title]" as={`/program/${program.short_title}`} passHref>
              <MenuLink to={`/program/${program.short_title}`}>{program.title}</MenuLink>
            </Link>
          ))
        ) : (
          <Loading />
        )}
      </DropDownMenu>
    </Menu>
  );

  const visitNotificationLinkAndMarkAsRead = (notification) => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markNotificationAsRead(input: {id: ${notification.id}}) {
              notification {
                id
                read
              }
            }
          }`,
      })
      .then((_response) => {
        // On success, update unread count
        notification.read = true;
        setUnreadNotificationsCount(unreadNotificationsCount - 1);
      })
      .catch((err) => {
        // do something
      });
  };

  const markAllNotificationsAsRead = () => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markAllNotificationsAsRead(input: {}) {
              unreadCount
            }
          }`,
      })
      .then((response) => {
        // On success, update unread count
        setUnreadNotificationsCount(response.data.unreadCount);
        let notifs = notifications.map((notif) => {
          notif.read = true;
          return notif;
        });
        setNotifications(notifs);
      })
      .catch((err) => {
        // do something
      });
  };

  const NotificationsDropdown = ({ size }) => (
    <Menu>
      <Box alignSelf="flex-end" mb="4px">
        <StyledMenuButton style={{ position: 'relative' }}>
          <FontAwesomeIcon icon="bell" style={{ fontSize: size }} />
          <Box
            position="absolute"
            right="2px"
            top="-9px"
            bg="#D11024"
            color="white"
            fontSize="0.72rem"
            fontWeight="600"
            borderRadius="4px"
            padding="0px 2px"
          >
            {unreadNotificationsCount > 0 && unreadNotificationsCount <= 99
              ? // if notif number is between 1 and 99, display count
                unreadNotificationsCount
              : // if more than 99, display 99+
                unreadNotificationsCount > 99 && '99+'}
          </Box>
        </StyledMenuButton>
      </Box>
      <NotifDropDown>
        <Box row justifyContent="space-between" alignItems="center" py={2} px={3} borderBottom="1px solid grey">
          <Box fontWeight="bold">
            <FormattedMessage id="settings.notifications.title" defaultMessage="Notifications" />
          </Box>
          <Box row borderTop="none" alignItems="flex-end" pl={2}>
            <Box borderTop="none" onClick={() => markAllNotificationsAsRead()}>
              <A href="#">
                <FormattedMessage id="settings.notifications.markAsRead" defaultMessage="Mark All as read" />
              </A>
            </Box>
            <Box px={1}>.</Box>
            <Box>
              <Link href="/user/[id]/settings" as={`/user/${user?.user?.id}/settings`}>
                <a>
                  <FormattedMessage id="menu.profile.settings" defaultMessage="Settings" />
                </a>
              </Link>
            </Box>
          </Box>
        </Box>
        <NotificationsList>
          {notifications.map((notification) => {
            return (
              <Notification
                as="a"
                href={notification.link}
                key={notification.id}
                hasRead={notification.read}
                onClick={() => visitNotificationLinkAndMarkAsRead(notification)}
              >
                {/* {!notification.read && <FontAwesomeIcon icon="circle" size="xs" />} */}
                <Box row flexWrap="wrap">
                  {/* <Box justifyContent="space-between" row> */}
                  <Box pr={3}>{notification.subjectLine}</Box>
                  <Box color="#797979">{displayObjectRelativeDate(notification.createdAt)}</Box>
                </Box>
              </Notification>
            );
          })}
        </NotificationsList>
        <Box alignItems="center" py={2} borderTop="1px solid grey">
          <Link href="/notifications" as="/notifications" passHref>
            <A>
              <FormattedMessage id="program.seeAll" defaultMessage="See all" />
            </A>
          </Link>
        </Box>
      </NotifDropDown>
    </Menu>
  );

  return (
    <Container height={10} px={[3]}>
      {/* for users navigating with keyboard, show this so they can skip content*/}
      <div className="skip-links">
        Skip to <a href="#main">content</a> or <a href="#footer">footer</a>
      </div>
      {/* Mobile Nav -- Will only display on mobile breakpoints */}
      <MobileNav display={['flex', undefined, undefined, 'none']}>
        <div>
          <Link href="/" as="/">
            <a>
              <Logo src="/images/logo_img.png" alt="JoGL icon" width={theme.sizes[11]} />
            </a>
          </Link>
        </div>
        <Box row>
          {user.isConnected && (
            <Box row spaceX={4} pr={2}>
              <NotificationsDropdown size="1.5rem" />
              <UserMenu />
            </Box>
          )}
          <Menu>
            <BurgerButton onClick={() => setOpen(!isOpen)}>
              <FontAwesomeIcon icon={faBars} />
            </BurgerButton>
            <Transition in={isOpen} timeout={300}>
              {(state) => (
                // state change: exited -> entering -> entered -> exiting -> exited
                <MobileSideNav open={state === 'entering' || state === 'entered'} p={4}>
                  <MobileLinksBox spaceY={3}>
                    {navLinks.map((link, i) => (
                      <Link href={link.url} as={link.as} key={i} passHref>
                        <AMenu>
                          <FormattedMessage id={link.intlId} defaultMessage={link.title} />
                        </AMenu>
                      </Link>
                    ))}
                    <Box pt={3}>
                      <ProgramsDropDown />
                    </Box>
                    <Link href="/search/[active-index]" as="/search/members" passHref>
                      <AMenu>
                        <FontAwesomeIcon icon={faSearch} />
                        <FormattedMessage id="header.search" defaultMessage="Search" />
                      </AMenu>
                    </Link>
                    {user.isConnected && (
                      <Box pt={3}>
                        <CreateObjectsDropdown />
                      </Box>
                    )}
                    <Box pt={3}>
                      <LangDropdown />
                    </Box>
                    {!user.isConnected && (
                      <Box row spaceX={2} alignItems="center">
                        <div className="signup">
                          <A href="/signup" as="/signup">
                            <FormattedMessage id="header.signUp" defaultMessage="Sign up" />
                          </A>
                          <FormattedMessage id="general.or" defaultMessage=" or " />
                        </div>
                        <Button onClick={gotoSignInPage}>
                          <FormattedMessage id="header.signIn" defaultMessage="Sign in" />
                        </Button>
                      </Box>
                    )}
                  </MobileLinksBox>
                </MobileSideNav>
              )}
            </Transition>
          </Menu>
        </Box>
      </MobileNav>
      {/* Desktop Nav */}
      <DesktopNav display={['none', undefined, undefined, 'flex']}>
        <Box row spaceX={[undefined, undefined, 4, 5]} display="inline-flex" alignItems="center">
          <Link href="/" as="/" passHref>
            <AMenu>
              <Logo src="/images/logo_img.png" alt="JoGL icon" width={theme.sizes[12]} />
            </AMenu>
          </Link>
          {navLinks.map((link, i) => (
            <Link href={link.url} as={link.as} key={i} passHref>
              <AMenu>
                <FormattedMessage id={link.intlId} defaultMessage={link.title} />
              </AMenu>
            </Link>
          ))}
          <ProgramsDropDown />
          <Link href="/search/[active-index]" as="/search/members" passHref>
            <AMenu>
              <FontAwesomeIcon icon={faSearch} />
              <FormattedMessage id="header.search" defaultMessage="Search" />
            </AMenu>
          </Link>
        </Box>
        <Box row spaceX={2}>
          {user.isConnected && <NotificationsDropdown size="1.2rem" />}
          {user.isConnected && <CreateObjectsDropdown />}
          <LangDropdown />
          {user.isConnected ? (
            <UserMenu />
          ) : (
            <Button onClick={gotoSignInPage}>
              <FormattedMessage id="header.signIn" defaultMessage="Sign in" />
            </Button>
          )}
        </Box>
      </DesktopNav>
    </Container>
  );
};

const CreateObjectsDropdown = () => (
  <Menu>
    <StyledMenuButton textAlign={['left', undefined, undefined, 'center']}>
      <FormattedMessage id="entity.form.btnCreate" defaultMessage="Create" /> <span aria-hidden>▾</span>
    </StyledMenuButton>
    <DropDownMenu>
      <Link href="/project/create" passHref>
        <MenuLink to="/project/create">
          <FormattedMessage id="header.createProject" defaultMessage="a project" />
        </MenuLink>
      </Link>
      <Link href="/community/create" passHref>
        <MenuLink to="/community/create">
          <FormattedMessage id="header.createGroup" defaultMessage="a group" />
        </MenuLink>
      </Link>
    </DropDownMenu>
  </Menu>
);

const LangDropdown = () => {
  const { locale } = useIntl();
  const changeLanguage = (newLanguage) => {
    // This is allowed because it is only called on client side.
    // This code has a lot of implications, because normally there's no locale set in the
    // cookies so the prioritized language is the one from your browser if it exists.
    // If your browser language is not supported it will fallback to english.
    // But if you set a locale in the cookies this lang will prioritize over all langs.
    // This is all defined in the custom server in server.js
    cookie.set('locale', newLanguage, { expires: 365 });
    window.location.reload();
  };
  return (
    <Menu>
      <StyledMenuButton textAlign={['left', undefined, undefined, 'center']} uppercase>
        <FontAwesomeIcon icon={faGlobe} /> {locale} <span aria-hidden>▾</span>
      </StyledMenuButton>
      <DropDownMenu>
        {languages.map((lang, i) => (
          <LangItem
            onClick={() => changeLanguage(lang.id)}
            onSelect={() => changeLanguage(lang.id)}
            selected={locale === lang.id}
            key={i}
          >
            {lang.title} {locale === lang.id && <FontAwesomeIcon icon={faCheck} />}
          </LangItem>
        ))}
      </DropDownMenu>
    </Menu>
  );
};

const BurgerButton = styled(StyledMenuButton)`
  z-index: 1;
  position: relative;
  margin-left: 0.75rem;
  font-size: 1.5rem;
`;

const NotifDropDown = styled(DropDownMenu)`
  border: ${(p) => `1px solid ${p.theme.colors.greys['700']}`};
  > div * {
    border-top: none;
    font-size: 0.9rem;
  }
  a {
    white-space: normal;
  }
  width: 400px;

  @media (max-width: 500px) {
    width: 330px;
  }
  @media (max-width: 450px) {
    width: 300px;
  }
  @media (max-width: 425px) {
    width: 280px;
  }
  @media (max-width: 400px) {
    width: 235px;
  }
  @media (max-width: 360px) {
    width: 215px;
  }
  @media (max-width: 330px) {
    width: 205px;
  }
`;

const NotificationsList = styled(Box)`
  overflow-y: scroll;
  height: 470px;
  max-height: 70vh;
  a {
    white-space: normal;
    font-size: 15px !important;
  }
`;

const Notification = styled(MenuLink)`
  background: ${(p) => (p.hasRead ? 'white' : '#EDF2FA')};
  border-bottom: ${(p) => `1px solid ${p.theme.colors.greys['500']}!important`};
  padding: 5px 12px;
  &:hover {
    background: ${(p) => (p.hasRead ? '#f6f6f6' : '#e2e7ee')};
    color: inherit;
  }
`;

const MobileLinksBox = styled(Box)`
  > * + * {
    ${(p) => `border-top: 1px solid ${p.theme.colors.greys['400']}!important`};
    padding-top: 0.75rem;
  }
`;

export default React.memo(Header);
