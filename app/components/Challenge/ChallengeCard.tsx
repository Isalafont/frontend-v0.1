/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { DataSource } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import { statusToStep } from './ChallengeStatus';
import ReactTooltip from 'react-tooltip';

export type Challenge = {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  clapsCount: number;
  membersCount: number;
  projectsCount: number;
  programId?: number;
  programShortTitle?: string;
  programTitle?: string;
  programTitleFr?: string;
  needsCount: number;
  has_saved: boolean;
  banner_url: string;
  status: string;
};
interface Props extends Challenge {
  width?: string;
  source?: DataSource;
  cardFormat?: string;
}
const ChallengeCard: FC<Props> = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  clapsCount,
  membersCount,
  projectsCount,
  programId,
  programShortTitle,
  programTitle,
  programTitleFr,
  needsCount = 0,
  has_saved,
  banner_url = '/images/default/default-challenge.jpg',
  width,
  source,
  cardFormat,
  status,
}) => {
  const { locale, formatMessage } = useIntl();
  const challengeUrl = { href: '/challenge/[short_title]', as: `/challenge/${short_title}` };
  const statusStep = statusToStep(status);
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';
  return (
    <Card imgUrl={banner_url} imgLinkObject={challengeUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={challengeUrl.href} as={challengeUrl.as} passHref>
          <Title pr={2}>
            <H2 fontSize={TitleFontSize}>{locale === 'fr' && title_fr ? title_fr : title}</H2>
          </Title>
        </Link>
        <BtnSave itemType="challenges" itemId={id} saveState={has_saved} source={source} />
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
        <span> Last active today </span> <span> Prototyping </span>
      </div> */}
      {programId &&
      programId !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
          <Box row mt={0}>
            <FormattedMessage id="entity.info.program.title" defaultMessage="Program: " />
            &nbsp;
            <Link href="/program/[short_title]" as={`/program/${programShortTitle}`}>
              <a>{locale === 'fr' && programTitleFr ? programTitleFr : programTitle}</a>
            </Link>
          </Box>
        )}
      <Box
        style={{ color: status !== 'draft' ? statusStep[1] : 'grey' }}
        row
        width="fit-content"
        borderRadius={6}
        mt={2}
        alignItems="center"
        data-tip={formatMessage({
          id: 'entity.info.status',
          defaultMessage: 'Status',
        })}
        data-for="challengeCard_status"
      >
        <Box>
          {formatMessage({
            id: `challenge.info.status.${status}`,
            defaultMessage: status,
          })}
        </Box>
      </Box>
      <ReactTooltip id="challengeCard_status" effect="solid" />
      <Box flex="1">{locale === 'fr' && short_description_fr ? short_description_fr : short_description}</Box>
      <Box row alignItems="center" justifyContent="space-between" spaceX={2} flexWrap="wrap">
        <CardData value={membersCount} title={textWithPlural('member', membersCount)} />
        <CardData value={needsCount} title={textWithPlural('need', needsCount)} />
        <CardData value={projectsCount} title={textWithPlural('project', projectsCount)} />
        <CardData value={clapsCount} title={textWithPlural('clap', clapsCount)} />
      </Box>
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

export default ChallengeCard;
