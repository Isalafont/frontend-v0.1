import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import DropdownRole from '../Tools/DropdownRole';
import { useApi } from '~/contexts/apiContext';
import Box from '../Box';
import Button from '../primitives/Button';
import A from '../primitives/A';

export default function MemberCard({
  member,
  itemId,
  itemType,
  isOwner,
  callBack = () => {
    console.warn('Missing callback');
  },
}) {
  const [sending, setSending] = useState(false);
  const api = useApi();

  const [showChangedMessage, setShowChangedMessage] = useState(false);

  const onRoleChanged = () => {
    setShowChangedMessage(true);
    setTimeout(() => {
      setShowChangedMessage(false);
    }, 1500);
  };
  const getRole = () => {
    let actualRole = 'member';
    if (!member.owner && !member.admin && !member.member) {
      actualRole = 'pending';
    }
    if (member.member) {
      actualRole = 'member';
    }
    if (member.owner) {
      actualRole = 'owner';
    } else if (member.admin) {
      actualRole = 'admin';
    }
    return actualRole;
  };
  const acceptMember = (newRole) => {
    const role = getRole();
    const jsonToSend = {
      user_id: member.id,
      previous_role: role,
      new_role: newRole,
    };
    setSending(true);
    api
      .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
      .then(() => {
        setSending(false);
        callBack();
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        setSending(false);
      });
  };
  const removeMember = () => {
    setSending(true);
    api
      .delete(`/api/${itemType}/${itemId}/members/${member.id}`)
      .then(() => {
        setSending(false);
        callBack();
      })
      .catch(() => {
        callBack();
        setSending(false);
      });
  };

  if (member) {
    const role = getRole();

    let imgTodisplay = '/images/default/default-user.png';
    if (member.logo_url_sm) {
      imgTodisplay = member.logo_url_sm;
    }

    const bgLogo = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };
    return (
      <div>
        <Box flexDirection={['column', 'row']} justifyContent={['flex-start', 'space-between']} key={member.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <A href="/user/[id]" as={`/user/${member.id}`}>
              {member.first_name} {member.last_name}
            </A>
          </Box>
          <Box row>
            {role === 'pending' ? (
              <>
                <button
                  type="button"
                  className="btn btn-outline-success"
                  disabled={sending === 'accept'}
                  style={{ marginBottom: 0 }}
                  onClick={() => acceptMember('member')}
                >
                  {sending === 'accept' && (
                    <>
                      <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="general.accept" defaultMessage="Accept" />
                </button>
                <button
                  type="button"
                  className="btn btn-outline-danger"
                  disabled={sending === 'remove'}
                  style={{ marginBottom: 0 }}
                  onClick={removeMember}
                >
                  {sending === 'remove' && (
                    <>
                      <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="general.reject" defaultMessage="Reject" />
                </button>
              </>
            ) : (
              <Box row width={['100%', '20rem']} alignItems="center">
                <Box width="100%">
                  <DropdownRole
                    onRoleChanged={onRoleChanged}
                    actualRole={role}
                    callBack={callBack}
                    itemId={itemId}
                    itemType={itemType}
                    // show different list roles depending if user is owner (to prevent admins changing their roles to "owner")
                    listRole={isOwner ? ['owner', 'admin', 'member'] : ['admin', 'member']}
                    member={member}
                    // don't show dropdown of roles if member you want to change is owner (except if you are the owner), to prevent admins changing owner's role.
                    isDisabled={role !== 'owner' || isOwner}
                  />
                </Box>
                {role !== 'owner' && isOwner && (
                  // show button only to object leaders/owners and if you are not removing an owner
                  <Button btnType="danger" disabled={sending} onClick={removeMember} ml={3}>
                    {sending && (
                      <>
                        <span
                          className="spinner-border spinner-border-sm text-center"
                          role="status"
                          aria-hidden="true"
                        />
                        &nbsp;
                      </>
                    )}
                    <FormattedMessage id="member.remove" defaultMessage="remove" />
                  </Button>
                )}
              </Box>
            )}
          </Box>
        </Box>
        {showChangedMessage && (
          <div
            className={`roleChangedMessage alert member${member.id} alert-success`}
            role="alert"
            style={{ marginTop: '7px' }}
          >
            <FormattedMessage id="member.role.changed" defaultMessage="role was updated" />
          </div>
        )}
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
