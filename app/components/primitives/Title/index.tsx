import styled from '~/utils/styled';
import { color, space, typography } from 'styled-system';

const Title = styled.a`
  ${[color, space, typography]};
  cursor: pointer;
  color: ${(p) => p.theme.colors.greys['800']};
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  :hover {
    color: ${(p) => p.theme.colors.greys['800']};
  }
`;
export default Title;
